<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="UTF-8">
    <title>Pandoc-mode by joostkremers</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="stylesheets/normalize.css" media="screen">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="stylesheets/stylesheet.css" media="screen">
    <link rel="stylesheet" type="text/css" href="stylesheets/github-light.css" media="screen">
  </head>
  <body>
    <section class="page-header">
      <h1 class="project-name">Pandoc-mode</h1>
      <h2 class="project-tagline">Emacs mode for interacting with Pandoc</h2>
      <a href="https://github.com/joostkremers/pandoc-mode" class="btn">View on GitHub</a>
      <a href="https://github.com/joostkremers/pandoc-mode/zipball/master" class="btn">Download .zip</a>
      <a href="https://github.com/joostkremers/pandoc-mode/tarball/master" class="btn">Download .tar.gz</a>
    </section>

    <section class="main-content">
      <h1>
<a id="introduction" class="anchor" href="#introduction" aria-hidden="true"><span class="octicon octicon-link"></span></a>Introduction</h1>

<p><code>pandoc-mode</code> is an Emacs mode for interacting with <a href="http://pandoc.org/">Pandoc</a>. Pandoc is a program (plus libraries) created by John MacFarlane that can convert a text written in one markup language into another markup language. <code>pandoc-mode</code> is implemented as a minor mode that can be activated alongside the major mode for any of Pandoc's supported input formats. It provides facilities to set the various options that Pandoc accepts and to run Pandoc on the input file. It is possible to create different output profiles for a single input file, so that you can, for example, write your text in Markdown and then translate it to HTML for online reading, PDF for offline reading and Texinfo for reading in Emacs, all with just a few keystrokes.</p>

<p>The current version of <code>pandoc-mode</code> is 2.13 and is compatible with Pandoc version 1.13.</p>

<h1>
<a id="installation" class="anchor" href="#installation" aria-hidden="true"><span class="octicon octicon-link"></span></a>Installation</h1>

<p>The easiest way to install <code>pandoc-mode</code> is to use the <a href="http://melpa.org/">Melpa</a> package repository. Alternatively, you can install it manually: download <code>pandoc-mode.el</code> and <code>pandoc-mode-utils.el</code>, put them into Emacs' loadpath (byte-compiling is recommended) and add the line <code>(load "pandoc-mode")</code> to your init file.</p>

<p>Either installation method allows Emacs to load <code>pandoc-mode</code>, but they do not activate it. In order to activate <code>pandoc-mode</code> in a buffer, you need to type <code>M-x pandoc-mode</code>. To start <code>pandoc-mode</code> automatically when you load e.g., a Markdown file, you can add a hook to your init file:</p>

<pre><code>(add-hook 'markdown-mode-hook 'pandoc-mode)
</code></pre>

<p>However, if you do not want to start <code>pandoc-mode</code> every time you work on a Markdown document, you can use a different function in <code>markdown-mode-hook</code>: instead of using <code>pandoc-mode</code>, you can use <code>conditionally-turn-on-pandoc</code>. This function checks if a default settings file exists for the file you're opening and only turns on <code>pandoc-mode</code> if it finds one. (For more info on the settings file, see the section <a href="#settings-files">'Settings Files'</a>.)</p>

<p>Additionally, if you want to automatically load the default <code>pandoc-mode</code> settings file for the file you're opening, you can add the following to your init file:</p>

<pre><code>(add-hook 'pandoc-mode-hook 'pandoc-load-default-settings)
</code></pre>

<p>The function <code>pandoc-load-default-settings</code> checks if a default settings file exists for the file being loaded and reads its settings if it finds one.</p>

<h2>
<a id="os-x" class="anchor" href="#os-x" aria-hidden="true"><span class="octicon octicon-link"></span></a>OS X</h2>

<p><code>pandoc-mode</code> expects that the <code>pandoc</code> binary can be found in your system’s <code>$PATH</code> and that the contents of <code>$PATH</code> is available to Emacs. Especially on OS X, this may not always be the case. To remedy this, you can set the user option <code>pandoc-binary</code> to the full path of <code>pandoc</code> on your system. A more elegant solution is to install the <a href="https://github.com/purcell/exec-path-from-shell">exec-path-from-shell</a> package. This package makes sure that the contents of your system’s <code>$PATH</code> variable is visible to Emacs, allowing Emacs to find <code>pandoc</code>.</p>

<h1>
<a id="usage" class="anchor" href="#usage" aria-hidden="true"><span class="octicon octicon-link"></span></a>Usage</h1>

<p><code>pandoc-mode</code> uses the <a href="https://github.com/abo-abo/hydra">hydra</a> package to create a keyboard-driven menu interface to all options and settings. The main menu is called by pressing <code>C-c /</code>. After that, everything should be self-explanatory. From the main menu, you can run <code>pandoc</code> on the buffer, view the output buffer and the current settings, set the input and output formats, and you can go to the options menu.</p>

<p>Note that if <code>menu-bar-mode</code> is active, <code>pandoc-mode</code> also provides a menu in the menu bar. It has the same structure as the hydra menu and it has the advantage that options that do not apply to the current file (due to its input or output format), are generally greyed out. On the other hand, the hydra menu shows the value of the options and allows you to change more than one option without reopening the menu. The menu bar menu disappears when you select an option, the hydra menu (generally) does not. Instead, it can be dismissed with <code>q</code>. Below, I talk about the hydra menu specifically, but most of what is said applies to the menu bar menu as well.</p>

<p>In the options menu, you can set options for running <code>pandoc</code> on your input file. All Pandoc options can be set from the menu (except for one or two that do not make sense, e.g., <code>--print-default-template</code>). Note that when you set options, they only apply to the current file <em>and</em> the current output format. When you open another file, or when you change the output format, all settings are reset to their default values. (There are ways to make settings more permanent, of course, as discussed below.)</p>

<h2>
<a id="input-and-output-formats" class="anchor" href="#input-and-output-formats" aria-hidden="true"><span class="octicon octicon-link"></span></a>Input and output formats</h2>

<p>The most important settings are the input and output formats. The input format is set automatically by Emacs on the basis of the major mode of the input file, but you can change it if you need to. The output format defaults to "Native Haskell", so most likely you will want to set it to something else before you run Pandoc. The input and output format menus also provide access to a submenu with the Markdown extensions that Pandoc supports.</p>

<p>As already stated, you may wish to use different output formats for a single input file. Most likely, the options that you want to pass to Pandoc will be different for each output format. To make this easier, <code>pandoc-mode</code> has the ability to save the settings for a specific output format. The main menu has an option “Settings files” (<code>C-c / s</code>), which takes you to a submenu where you can save the current settings. Emacs saves these settings to a hidden file in the same directory as the file you're editing, under a name composed of the input file, appended with the name of the output format and the string <code>.pandoc</code>. So if your input file is called <code>mytext.md</code>, the <code>html</code> settings file will be called <code>.mytext.md.html.pandoc</code>. (See the section <a href="#settings-files">'Settings Files'</a> for details.)</p>

<p>A single document can have a separate settings file for each output format that Pandoc supports. These can simply be created by setting all options the way you want them for the first output format, save them, then choose another output format, set the required options, save again, etc. Because the name of a settings file contains the output format for which it was created, the different settings files won't interfere with each other. When you switch the output format (with <code>C-c / O</code>), Emacs checks if a corresponding settings file exists and loads it if one is found.</p>

<p>On systems that have symbolic links, it is also possible to specify a default output format (<code>C-c / s d</code>). This is done by creating a symbolic link to the settings file of the current output format (a settings file is created if one doesn't exist yet) with the output format replaced by the string <code>"default"</code>. The file it points to is read by the function <code>pandoc-load-default-settings</code>, making it possible to automatically load a specific settings file when <code>pandoc-mode</code> is invoked, as described above.</p>

<p>Note that the current output format is always visible in the mode line: the "lighter" for <code>pandoc-mode</code> in the mode line has the form <code>Pandoc/&lt;format&gt;</code>, where <code>&lt;format&gt;</code> is the current output format.</p>

<p>The major modes for which <code>pandoc-mode</code> selects an input format automatically can be customised (user option <code>pandoc-major-modes</code>). You can add major modes or remove those that you don't use. Similarly, you can customise the file extensions for each output format (<code>pandoc-output-format-extensions</code>).</p>

<h2>
<a id="the-options-menu" class="anchor" href="#the-options-menu" aria-hidden="true"><span class="octicon octicon-link"></span></a>The options menu</h2>

<p>The options menu has a number of submenus, each related to a specific type of options: file options, reader options, writer options (general and specific), citations and math rendering. The file options menu contains options for the output file, output directory, data directory, the directory to extract media files to, and the master file. Only two of these (the data directory and the extract media directory) correspond directly to a Pandoc option. The output file and output directory options are combined to form Pandoc’s <code>--output</code> option, while the master file option is only used by <code>pandoc-mode</code>. These options are discussed in the sections <a href="#setting-an-output-file">'Setting an output file'</a> and <a href="#master-file">’Master file’</a>, respectively.</p>

<p>Note that the subdivision in the options menu is based on the subdivision in the Pandoc README and the user guide on <a href="http://pandoc.org/README.html">Pandoc.org</a>, which should make it easier to find the relevant options in the menus.</p>

<p>One nice thing about the hydra menus is that the value of an option is displayed beside it. Pandoc’s options come in several different kinds. Switches, (boolean options that do not take a value), are toggled when you select them, and their value is displayed as either “yes” or “no”. If you select another kind of option, you are asked to provide a value in the minibuffer. For template variables and metadata items, you are asked both a variable / metadata name and a value.</p>

<p>Unsetting an option can usually be done by prefixing the relevant key with a dash <code>-</code>. This is actually the negative prefix argument, which can be typed without the meta (alt) key when inside a hydra menu. So for example, if you’re in the files menu (<code>C-c / o f</code>), you can set an output file with <code>o</code>. To unset the output file, type <code>- o</code>.</p>

<p>Many Pandoc options have file names as values. When <code>pandoc-mode</code> calls <code>pandoc</code>, it expands filenames, so that they are absolute and don't contain any abbreviations (such as <code>~</code> for one's home directory). This means you can have relative filenames in your settings, or indeed <code>~</code>, which can be practical if you move settings files to different locations or e.g. between computers with different OSes. (For example, Linux expands <code>~</code> to <code>/home/&lt;user&gt;</code>, while on OS X it becomes <code>/Users/&lt;user&gt;</code>.)</p>

<p>The CSS style sheet is an exception to this: <code>pandoc-mode</code> always cuts off the directory part of the filename you specify as CSS style sheet and doesn't expand it. The reason for this is that the CSS style sheet will normally be transferred along with the output file(s) to a server, where it will most likely be in a different directory than on the computer you're generating your HTML files on.</p>

<p>To get an overview of all the settings for the current file and output format, you can use the option “View current settings” in the main menu (<code>C-c / S</code>). This displays all settings in the <code>*Pandoc output*</code> buffer in a Lisp-like format. For example, the settings for TeXinfo output of this manual look like this (unset options omitted):</p>

<pre><code>((standalone . t)
 (read . "markdown")
 (write . "texinfo")
 (output . t)
 (include-before-body . "~/src/pandoc-mode/manual/texi-before-body"))
</code></pre>

<h2>
<a id="template-variables-and-metadata" class="anchor" href="#template-variables-and-metadata" aria-hidden="true"><span class="octicon octicon-link"></span></a>Template variables and metadata</h2>

<p><code>pandoc-mode</code> allows you to set or change template variables through the menu. The variables are in the general writer options menu, the metadata in the reader options menu. Emacs will ask you for the name of a variable or metadata item and for a value for it. If you provide a name that already exists (TAB completion works), the new value replaces the old one.</p>

<p>Deleting a template variable or metadata item can be done by prefixing the menu key with <code>-</code>. Emacs will ask you for the variable name (TAB completion works here, too) and removes it from the list.</p>

<h2>
<a id="running-pandoc" class="anchor" href="#running-pandoc" aria-hidden="true"><span class="octicon octicon-link"></span></a>Running Pandoc</h2>

<p>The first item in the menu is "Run Pandoc" (accessible with <code>C-c / r</code>), which, as the name suggests, runs Pandoc on the document, passing all options you have set. By default, Pandoc sends the output to stdout, which is redirected to the buffer <code>*Pandoc output*</code>. (Except when the output format is "odt", "epub" or "docx", in which case output is always sent to a file.) The output buffer is not normally shown, but you can make it visible through the menu or by typing <code>C-c / V</code>. Error messages from Pandoc are also displayed in this buffer.</p>

<p>Note that when you run Pandoc, Pandoc doesn't read the file on disk. Rather, Emacs feeds it the contents of the buffer through <code>stdin</code>. This means that you don't actually have to save your file before running Pandoc. Whatever is in your buffer, saved or not, is passed to Pandoc. Alternatively, if the region is active, only the region is sent to Pandoc.</p>

<p>If you call this command with a prefix argument <code>C-u</code> (so the key sequence becomes <code>C-/ C-u r</code>: <code>C-/</code> to open the menu and <code>C-u r</code> to run Pandoc), Emacs asks you for an output format to use. If there is a settings file for the format you specify, the settings in it will be passed to Pandoc instead of the settings in the current buffer. If there is no settings file, Pandoc will be called with just the output format and no other options.</p>

<p>Note that specifying an output format this way does not change the output format or any of the settings in the buffer, it just changes the output profile used for calling Pandoc. This can be useful if you use different output formats but don't want to keep switching between profiles when creating the different output files.</p>

<h2>
<a id="setting-an-output-file" class="anchor" href="#setting-an-output-file" aria-hidden="true"><span class="octicon octicon-link"></span></a>Setting an output file</h2>

<p>If you want to save the output to a file rather than have it appear in the output buffer, you can set an explicit output file. Note that setting an output <em>file</em> is not the same thing as setting an output <em>format</em> (though normally the output file has a suffix that indicates the format of the file).</p>

<p>In <code>pandoc-mode</code>, the output file setting has three options: the default is to send output to stdout, in which case it is redirected to the buffer <code>*Pandoc output*</code>. This option can be selected by typing <code>- o</code> in the file options menu. Alternatively, you can let Emacs create an output filename for you. In this case the output file will have the same base name as the input file but with the proper suffix for the output format. To select this option, prefix the output file key <code>o</code> with <code>C-u</code> in the file options menu. The third option is to specify an explicit output file. This can (obviously) be done by hitting just <code>o</code>.</p>

<p>Note that Pandoc does not allow output to be sent to stdout if the output format is an OpenOffice.org Document (ODT), EPUB or MS Word (docx) file. Therefore, Emacs will always create an output filename in those cases, unless of course you've explicitly set an output file yourself.</p>

<p>The output file you set is always just the base filename, it does not specify a directory. Which directory the output file is written to depends on the setting "Output Directory" (which is not actually a Pandoc option). Emacs creates an output destination out of the settings for the output directory and output file. If you don't specify any output directory, the output file will be written to the same directory that the input file is in.</p>

<h2>
<a id="creating-a-pdf" class="anchor" href="#creating-a-pdf" aria-hidden="true"><span class="octicon octicon-link"></span></a>Creating a pdf</h2>

<p>The second item in the main menu is "Create PDF" (invoked with <code>C-c / p</code>). This option calls Pandoc with an output file with the extention <code>.pdf</code>, causing Pandoc to create a pdf file by first converting to <code>.tex</code> and then calling LaTeX on it. Like <code>C-c / r</code>, this command operates on the region if it is active.</p>

<p>If you choose this option, Emacs checks if your current output format is <code>latex</code>. If it is, Emacs calls Pandoc with the buffer's settings. If the output format is something other than <code>latex</code>, Emacs checks if you have a settings file for LaTeX output and uses those settings. This allows you to create a pdf without having to switch the output format to LaTeX. If your current output format is not LaTeX and no LaTeX settings file is found, Emacs calls Pandoc with only the input and output formats.</p>

<h1>
<a id="font-lock" class="anchor" href="#font-lock" aria-hidden="true"><span class="octicon octicon-link"></span></a>Font lock</h1>

<p><code>pandoc-mode</code> adds font lock keywords for citations and numbered example lists. The relevant faces can be customised in the customisation group <code>pandoc</code>.</p>

<h1>
<a id="settings-files" class="anchor" href="#settings-files" aria-hidden="true"><span class="octicon octicon-link"></span></a>Settings Files</h1>

<p>Apart from settings files for individual files (which are called <em>local settings files</em>), <code>pandoc-mode</code> supports two other types of settings files: project files and global files. Project files are settings files that apply to all input files in a given directory (except those files for which a local settings file exists). Global settings files, as the name implies, apply globally, to files for which no local or project file is found. Both types of files are specific to a particular output format, just like local settings files. Project files live in the directory they apply to and are called <code>Project.&lt;format&gt;.pandoc</code>. Global files live in the directory specified by the variable <code>pandoc-data-dir</code>, which defaults to <code>~/.emacs.d/pandoc-mode/</code>, but this can of course be changed in the customisation group <code>pandoc</code>.</p>

<p>Whenever <code>pandoc-mode</code> loads settings for an input file, it first checks if there is a local settings file. If none is found, it looks for a project file, and if that isn't found, it tries to load a global settings file. In this way, local settings override project settings and project settings override global settings. Note, however, that if a local settings file exists, <em>all</em> settings are read from this file. Any project file or global file for the relevant output format is ignored.</p>

<p>You can create a project or global settings file through the menu in the submenu "Settings Files". This simply saves all settings for the current buffer to a project or global settings file. (Any local settings file for the file in the current buffer will be kept. You'll need to delete it manually if you no longer need it.)</p>

<p>Note that starting with version 2.5, <code>pandoc-mode</code> settings files are written in a Lisp format (as demonstrated above). Old-style settings files continue to be read, so there is no need to change anything, but if you change any settings and save them, the file is converted.</p>

<h1>
<a id="file-local-variables" class="anchor" href="#file-local-variables" aria-hidden="true"><span class="octicon octicon-link"></span></a>File-local variables</h1>

<p><code>pandoc-mode</code> also allows options to be set as file-local variables, which gives you the ability to keep the settings for a file in the file itself. To specify an option in this way, use the long form of the option as a variable name, prefixed with <code>pandoc/</code> (note the slash; use <code>pandoc/read</code> and <code>pandoc/write</code> for the input and output formats, and <code>pandoc/table-of-contents</code> for the TOC).</p>

<p>For example, in order to set a bibliography file, add the following line to the local variable block:</p>

<pre><code>pandoc/bibliography: "~/path/to/mybib.bib"
</code></pre>

<p>The easiest way to add a file-local variable is to use the command <code>M-x add-file-local-variable</code>. This will put the variable at the end of the file and add the correct comment syntax. Note that the values are Lisp expressions, which means that strings need to be surrounded with double quotes. Symbols do not need to be quoted, however.</p>

<p>Settings specified as file-local variables are kept separate from other settings: they cannot be set through the menu, they are <em>never</em> saved to a settings file, and they are not shown when you call <code>pandoc-view-settings</code> (<code>C-c / S</code>). A source file can both have a settings file and specify settings in file-local variables. If this happens, the latter override the former</p>

<p>Note that it is also possible to specify the customisation option <code>pandoc-binary</code> as a file-local variable. It does not require the <code>pandoc/</code> prefix, but since its value is a string, it must be enclosed in quotes:</p>

<pre><code>pandoc-binary: "/path/to/alternate/pandoc“
</code></pre>

<h1>
<a id="managing-numbered-examples" class="anchor" href="#managing-numbered-examples" aria-hidden="true"><span class="octicon octicon-link"></span></a>Managing numbered examples</h1>

<p>Pandoc provides a method for creating examples that are numbered sequentially throughout the document (see <a href="http://pandoc.org/README.html#numbered-example-lists">Numbered example lists</a> in the Pandoc documentation). <code>pandoc-mode</code> makes it easier to manage such lists. First, by going to "Example Lists | Insert New Example" (<code>C-c / e i</code>), you can insert a new example list item with a numeric label: the first example you insert will be numbered <code>(@1)</code>, the second <code>(@2)</code>, and so on. Before inserting the first example item, Emacs will search the document for any existing definitions and number the new items sequentially, so that the numeric label will always be unique.</p>

<p>Pandoc allows you to refer to such labeled example items in the text by writing <code>(@1)</code> and <code>pandoc-mode</code> provides a facility to make this easier. If you select the menu item "Example Lists | Select And Insert Example Label" (<code>C-c / e s</code>) Emacs displays a list of all the <code>(@)</code>-definitions in your document. You can select one with the up or down keys (you can also use <code>j</code> and <code>k</code> or <code>n</code> and <code>p</code>) and then hit <code>return</code> to insert the label into your document. If you change your mind, you can leave the selection buffer with <code>q</code> without inserting anything into your document.</p>

<h1>
<a id="using--directives" class="anchor" href="#using--directives" aria-hidden="true"><span class="octicon octicon-link"></span></a>Using @@-directives</h1>

<p><code>pandoc-mode</code> includes a facility to make specific, automatic changes to the text before sending it to Pandoc. This is done with so-called <code>@@</code>-directives, which trigger an Elisp function and are then replaced with the output of that function. A <code>@@</code>-directive takes the form <code>@@directive</code>, where <code>directive</code> can be any user-defined string (see <a href="#defining--directives">how to define directive strings</a>). Before Pandoc is called, Emacs searches the text for these directives and replaces them with the output of the functions they call.</p>

<p>So suppose you define (e.g., in <code>~/.emacs.d/init</code>) a function <code>my-pandoc-current-date</code>:</p>

<pre><code>(defun my-pandoc-current-date (_)
  (format-time-string "%d %b %Y"))
</code></pre>

<p>Now you can define a directive <code>@@date</code> that calls this function. The effect is that every time you write <code>@@date</code> in your document, it is replaced with the current date.</p>

<p>Note that the function that the directive calls must have one argument. which is used to pass the output format to the function (as a string). This way you can have your directives do different things depending on the output format. This argument can be called anything you like. In the above example, it is called <code>_</code> (i.e., just an underscore), to indicate that the variable is not actually used in the function. If you do use it, you should probably choose a more meaningful name.</p>

<p><code>@@</code>-directives can also take the form <code>@@directive{...}</code>. Here, the text between curly braces is an argument, which is passed to the function called by the directive as the second argument. Note that there should be <em>no</em> space between the directive and the left brace. If there is, Emacs won't see the argument and will treat it as normal text.</p>

<p>It is possible to define a directive that can take an optional argument. This is simply done by defining the argument that the directive's function takes as optional. Suppose you define <code>my-pandoc-current-date</code> as follows:</p>

<pre><code>(defun my-pandoc-current-date (_ &amp;optional text)
  (format "%s%s" (if text (concat text ", ") "")
                 (format-time-string "%d %b %Y")))
</code></pre>

<p>This way, you could write <code>@@date</code> to get just the date, and <code>@@date{Cologne}</code> to get "Cologne, 30 Okt 2015".</p>

<p>Two directives have been predefined: <code>@@lisp</code> and <code>@@include</code>. Both of these take an argument. <code>@@lisp</code> can be used to include Elisp code in the document which is then executed and replaced by the result (which should be a string). For example, another way to put the current date in your document, without defining a special function for it, is to write the following:</p>

<pre><code>@@lisp{(format-time-string "%d %b %Y")}
</code></pre>

<p>Emacs takes the Elisp code between the curly braces, executes it, and replaces the directive with the result of the code. Note that the code can be anything, and there is no check to see if it is “safe”.</p>

<p><code>@@include</code> can be used to include another file into the current document (which must of course have the same input format):</p>

<pre><code>@@include{copyright.text}
</code></pre>

<p>This directive reads the file <code>copyright.text</code> and replaces the <code>@@include</code> directive with its contents.</p>

<p>Processing <code>@@</code>-directives works everywhere in the document, including in code and code blocks, and also in the %-header block. So by putting the above <code>@@lisp</code> directive in the third line of the %-header block, the meta data for your documents will always show the date on which the file was created by Pandoc.</p>

<p>If it should ever happen that you need to write a literal <code>"@@lisp"</code> in your document, you can simply put a backslash \ before the first <code>@</code>: <code>\@@lisp</code>. Emacs removes the backslash (which is necessary in case the string <code>\@@lisp</code> is contained in a code block) and then continues searching for the next directive.</p>

<p>After Emacs has processed a directive and inserted the text it produced in the buffer, processing of directives is resumed from the <em>start</em> of the inserted text. That means that if an <code>@@include</code> directive produces another <code>@@include</code> directive, the newly inserted <code>@@include</code> directive gets processed as well.</p>

<h2>
<a id="master-file" class="anchor" href="#master-file" aria-hidden="true"><span class="octicon octicon-link"></span></a>Master file</h2>

<p>If you have a master file with one or more <code>@@include</code> directives and you’re editing one of the included files, running Pandoc from that buffer will not produce the desired result, because it runs Pandoc on the included file. To make working with included files easier, you can specify a master file for them, with the command <code>pandoc-set-master-file</code> (through the menu with <code>C-c / o f m</code>). When this option is set, Pandoc is run on the master file rather than on the file in the current buffer.</p>

<p>The settings used in this case are always the settings for the master file, not the settings for the included file. The only exception is the output format, which is taken from the buffer from which you run Pandoc. This makes it possible to change the output format while in a buffer visiting an included file and have <code>pandoc-mode</code> do the right thing.</p>

<p>One thing to keep in mind is that the master file setting is dependent on the output format. When you set a master file, it is only set for the output format that is active. This means that you need to set the output format <em>before</em> you set the master file.</p>

<p>Note that the master file menu also has an option “Use this file as master file” (<code>C-c / o f M</code>). When you select this option, the current file is set as master file and a project settings file is created for the current output format. This is a quick way to set the master file for all files in a directory, since the project settings will apply to all files in the directory.</p>

<h2>
<a id="defining--directives" class="anchor" href="#defining--directives" aria-hidden="true"><span class="octicon octicon-link"></span></a>Defining @@-directives</h2>

<p>Defining <code>@@</code>-directives yourself is done in two steps. First, you need to define the function that the directive will call. This function must take at least one argument to pass the output format and may take at most one additional argument. It should return a string, which is inserted into the buffer. The second step is to go to the customisation buffer with <code>M-x customize-group</code> <code>RET</code> <code>pandoc</code> <code>RET</code>. One of the options there is <code>pandoc-directives</code>. This variable contains a list of directives and the functions that they are linked with. You can add a directive by providing a name (without <code>@@</code>) and the function to call. Note that directive names may only consists of letters (<code>a-z</code>, <code>A-Z</code>) or numbers (<code>0-9</code>). Other characters are not allowed. Directive names are case sensitive, so <code>@@Date</code> is not the same as <code>@@date</code>.</p>

<p>Passing more than one argument to an <code>@@</code>-directive is not supported. However, if you really want to, you could use <code>split-string</code> to split the argument of the <code>@@</code>-directive and "fake" multiple arguments that way.</p>

<p>A final note: the function that processes the <code>@@</code>-directives is called <code>pandoc-process-directives</code> and can be called interactively. This may be useful if a directive is not producing the output that you expect. By running <code>pandoc-process-directives</code> interactively, you can see what exactly your directives produce before the resulting text is sent to pandoc. The changes can of course be undone with <code>M-x undo</code> (usually bound to <code>C-/</code>).</p>

<h2>
<a id="directive-hooks" class="anchor" href="#directive-hooks" aria-hidden="true"><span class="octicon octicon-link"></span></a>Directive hooks</h2>

<p>There is another customisable variable related to <code>@@</code>-directives: <code>pandoc-directives-hook</code>. This is a list of functions that are executed <em>before</em> the directives are processed. These functions are not supposed to change anything in the buffer, they are intended for setting up things that the directive functions might need.</p>

<h1>
<a id="disabling-the-hydra-menu" class="anchor" href="#disabling-the-hydra-menu" aria-hidden="true"><span class="octicon octicon-link"></span></a>Disabling the hydra menu</h1>

<p>The hydra package provides a nice way to control <code>pandoc-mode</code> and to set all the options that Pandoc provides. However, if for some reason you prefer to use normal key bindings, you can disable the hydra menu by rebinding <code>C-c /</code>. To restore the original key bindings, put the following in your init file:</p>

<pre><code>(with-eval-after-load 'pandoc-mode
  (define-key 'pandoc-mode-map "C-c / r" #'pandoc-run-pandoc)
  (define-key 'pandoc-mode-map "C-c / p" #'pandoc-convert-to-pdf)
  (define-key 'pandoc-mode-map "C-c / s" #'pandoc-save-settings-file)
  (define-key 'pandoc-mode-map "C-c / w" #'pandoc-set-write)
  (define-key 'pandoc-mode-map "C-c / f" #'pandoc-set-master-file)
  (define-key 'pandoc-mode-map "C-c / m" #'pandoc-set-metadata)
  (define-key 'pandoc-mode-map "C-c / v" #'pandoc-set-variable)
  (define-key 'pandoc-mode-map "C-c / V" #'pandoc-view-output)
  (define-key 'pandoc-mode-map "C-c / S" #'pandoc-view-settings)
  (define-key 'pandoc-mode-map "C-c / c" #'pandoc-insert-@)
  (define-key 'pandoc-mode-map "C-c / C" #'pandoc-select-@))
</code></pre>

<p>It’s also possible to bind other commands to keys. The switches (i.e., the options that can only be on or off) can be toggled with the command <code>pandoc-toggle-interactive</code>. All other options (except <code>--read</code>) have dedicated functions to set them, called <code>pandoc-set-&lt;option&gt;</code>, where <code>&lt;option&gt;</code> corresponds to the long form of the option without the double dashes (use <code>write</code> rather than <code>to</code>, and <code>table-of-contents</code> rather than <code>toc</code>).</p>

      <footer class="site-footer">
        <span class="site-footer-owner"><a href="https://github.com/joostkremers/pandoc-mode">Pandoc-mode</a> is maintained by <a href="https://github.com/joostkremers">joostkremers</a>.</span>

        <span class="site-footer-credits">This page was generated by <a href="https://pages.github.com">GitHub Pages</a> using the <a href="https://github.com/jasonlong/cayman-theme">Cayman theme</a> by <a href="https://twitter.com/jasonlong">Jason Long</a>.</span>
      </footer>

    </section>

  
  </body>
</html>
