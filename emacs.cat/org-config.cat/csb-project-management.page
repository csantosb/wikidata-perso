---
format: Org
categories: emacs config projects
toc: yes
title: csb-project-management
content: emacs config for project management
...

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[#header][Header]]
- [[#tags-related][TAGS related]]
- [[#find-file-in-project][Find file in project]]
- [[#perspective][Perspective]]
  - [[#init][Init]]
  - [[#keys][Keys]]
  - [[#variables][Variables]]
  - [[#old-code][Old code]]
- [[#projectile][Projectile]]
  - [[#config][Config]]
  - [[#helm-projectile][Helm-projectile]]
  - [[#develop][Develop]]
  - [[#features][Features]]
  - [[#references][References]]
- [[#persp-projectile][Persp Projectile]]
- [[#helm-perso-wiki][Helm perso wiki]]
- [[#personal-workflow][Personal Workflow]]
  - [[#config-file][Config file]]
  - [[#project-files-for-prj-project-name][Project files for 'prj' project NAME]]
  - [[#variable-and-constant-declaration][Variable and constant declaration]]
  - [[#launcher-function][Launcher function]]
  - [[#helper-functions][Helper functions]]
- [[#trailer][Trailer]]
- [[#references-1][References]]

* Header

#+begin_src emacs-lisp
;;; csb-project-management.el --- Summary
;;
;;; License:
;;
;;; Commentary:
;;
;; This file contains defaults for variables as well as global keystrokes
;;
;;; Code:
#+end_src

* TAGS related

#+begin_src emacs-lisp
  (stante-after ggtags
                (org-require 'csb-project-management-ggtags t))
#+end_src

* Find file in project

#+begin_src emacs-lisp :tangle no
  (setq ffip-use-rust-fd t)
#+end_src

* Perspective

Allows organizing buffers under “tabs” or “perspectives”. I am using one
perspective by project, with the “init” perspective as “scratch”.

The fix to avoid problems with Emacs 26 removing “frame-local” is set in the
init [[file:/emacs.cat/org-config.cat/csb-init#Boot][page]].

** Init

#+begin_src emacs-lisp
  (add-hook 'after-init-hook
            (lambda()
              (persp-mode 1)))
#+end_src

#+begin_src emacs-lisp :tangle no
  (when (not (string= (and (daemonp) (daemonp)) "default"))
    (message "YES")
    (defun persp-switch (&optional arg))
    (defun persp-set-buffer (&optional arg))
    (defun persp-switch-to-buffer (&optional arg))
    (defun persp-kill (&optional arg)))
#+end_src

** Keys

#+begin_src emacs-lisp
  (stante-after perspective
                ;; swithching
                (define-key persp-mode-map (kbd "C-x x S") #'persp-switch-quick)
                (define-key persp-mode-map (kbd "C-¿") 'persp-switch-quick)
                (define-key persp-mode-map (kbd "M-SPC") #'persp-switch)
                (define-key persp-mode-map (kbd "C-x x n") #'persp-next)
                (define-key persp-mode-map (kbd "C-x x p") #'persp-prev)

                ;; killing
                (defun csb/projectile-perspective-kill()
                  ""
                  (interactive)
                  (mapc (lambda(arg) (kill-buffer arg)) (persp-buffers (persp-curr)))
                  (persp-kill (persp-name (persp-curr))))

                ;; (define-key persp-mode-map (kbd "C-x x k") #'persp-kill)
                (define-key persp-mode-map (kbd "C-x x k") #'csb/projectile-perspective-kill)
                (key-chord-define persp-mode-map (kbd "xk") #'csb/projectile-perspective-kill)

                (define-key persp-mode-map (kbd "C-x x A") 'persp-set-buffer)
                (define-key persp-mode-map (kbd "C-x x a") 'persp-add-buffer)
                (define-key persp-mode-map (kbd "C-x x d") 'persp-remove-buffer)
                (define-key persp-mode-map (kbd "C-x x i") 'persp-import-buffers)
                (define-key persp-mode-map (kbd "C-x x ,") 'persp-rename)

                ;; (define-key persp-mode-map (kbd "C-x x w") 'persp-save-state-to-file)
                ;; (define-key persp-mode-map (kbd "C-x x l") 'persp-load-state-from-file)
                (define-key persp-mode-map (kbd "C-x x <left>") nil)
                (define-key persp-mode-map (kbd "C-x x <right>") nil)
                (define-key persp-mode-map (kbd "C-x x c") nil))
#+end_src

** Variables

#+begin_src emacs-lisp
  (stante-after perspective
                (setq persp-show-modestring t)
                (if running-os-is-linux
                    (setq persp-interactive-completion-function 'ido-completing-read)
                  (setq persp-interactive-completion-function 'completing-read))
                (setq persp-initial-frame-name "init")
                (setq persp-modestring-dividers '(" " " " "  "))
                ;; (setq persp-modestring-dividers '("[" "]" "|"))
                )
#+end_src

** Old code

;; (defun persp-set-buffer(buffer-name)
  ;;   "Add BUFFER-NAME to current perspective and remove it from any other."
  ;;   (cond ((get-buffer buffer-name)
  ;;          (persp-add-buffer buffer-name)
  ;;          (while (persp-buffer-in-other-p (get-buffer buffer-name))
  ;;            (setq current-perspective persp-curr)
  ;;            (persp-switch (cdr (persp-buffer-in-other-p (get-buffer buffer-name))))
  ;;            (persp-remove-buffer buffer-name)
  ;;            (persp-switch (persp-name current-perspective))))
  ;;         (t (message "buffer %s doesn't exist" buffer-name))))


  ;;  (add-hook 'persp-switch-hook '(lambda()(when (projectile-project-p)
  ;;                                      (setq helm-perso-wiki-directory (replace-regexp-in-string "/$" ""
  ;;                                                                                                (projectile-project-p))))))

  ;; (add-hook 'persp-kill-hook #'(lambda()(when (projectile-project-p)
  ;;                                    (wg-save-session)
  ;;                                    (wg-kill-workgroup (wg-workgroup-name (wg-current-workgroup)))
  ;;                                    ;; (wg-kill-workgroup (persp-name persp-curr))
  ;;                                    ;; (wg-workgroup-name (wg-current-workgroup))
  ;;                                    ;; (wg-switch-to-workgroup (file-name-nondirectory (substring (projectile-project-p) 0 -1)))
  ;;                                    )))

  ;; (add-hook 'persp-switch-hook #'(lambda()(when (projectile-project-p)
  ;;                                       (setq wg-session-file (concat (projectile-project-p) ".workgroups"))
  ;;                                       (wg-open-session wg-session-file)
  ;;                                       (when (not (string= (wg-workgroup-name (wg-current-workgroup)) (persp-name persp-curr)))
  ;;                                         (wg-switch-to-workgroup (persp-name persp-curr)))
  ;;                                       ))

  ;; (defun csb/persp-switch(name)
  ;;   (interactive "i")
  ;;   (wg-save-session)
  ;;   ;;(when (wg-workgroup-name (wg-current-workgroup))
  ;;   ;;  )
  ;;   ;; (wg-current-workgroup-p "wikidata")
  ;;   (persp-switch name))

* Persp Projectile

Perspective integration with Projectile

#+begin_src emacs-lisp
  (use-package persp-projectile
    :after projectile
    :bind
    (:map persp-mode-map
          ("C-x x s" . projectile-persp-switch-project)))
#+end_src

* Projectile

** Config

#+begin_src emacs-lisp
  (use-package projectile
    :init (projectile-mode 1)

    ;; Keys
    :bind
    (:map projectile-mode-map
          ;; ("C-c p <return>" . csb/projectile-todo)
          ;; ("C-c p s r" . projectile-ripgrep)
          ;; ("C-c p s a" . projectile-ag)
          ("C-c p" . projectile-command-map)
          ;; ("C-c p p" . projectile-switch-project)
          ;; ("C-c p <backspace>" . csb/projectile-readme)
          )

    :config
    (add-to-list 'projectile-globally-ignored-directories ".backups")
    (add-to-list 'projectile-globally-ignored-directories ".stversions")

    ;; Variables
    (setq

     projectile-cache-file
     (if (daemonp) ;;(string= (and (daemonp) (daemonp)) "default")
         (format "%sprojectile.cache-%s" user-emacs-directory (daemonp))
       (format "%sprojectile.cache" user-emacs-directory))
     ;; (make-temp-file (format "/tmp/projectile.cache-"))

     projectile-completion-system 'helm

     projectile-globally-ignored-files '("TAGS" "GTAGS" "GPATH" "GRTAGS")
     ;; projectile-indexing-method 'alien

     projectile-enable-caching t
     ;; projectile-tags-command "ctags -Re -f %s %s" ;; doesn't matter if using gtags

     projectile-remember-window-config t

     projectile-mode-line-prefix  "P"
     ;; projectile-mode-line '(:eval
     ;; 		       (format " Projectile" ))
     ;; projectile-dired, projectile-find-file, helm-projectile
     projectile-switch-project-action 'projectile-dired ;; helm-projectile-find-file

     ;; Idle timer: when ‘projectile-enable-idle-timer’ is non-nil, the hook
     ;; ‘projectile-idle-timer-hook’ is run each time Emacs has been idle
     ;; for ‘projectile-idle-timer-seconds’ seconds and we’re in a project.
     projectile-enable-idle-timer t
     projectile-idle-timer-seconds 10
     projectile-idle-timer-hook '(projectile-regenerate-tags))

    ;; Ancillary functions: todo and readme files

    ;; Open project's TODO file
    (defun csb/projectile-todo()
      (interactive)
      (let ((todofile (format "%sTODO.org" (projectile-project-root)))
            (releasesfile (format "%sReleases.org" (projectile-project-root))))
        (if (file-exists-p releasesfile)
            (find-file-other-window releasesfile)
          (find-file-other-window todofile))))

    ;; Open project's README file
    (defun csb/projectile-readme()
      (interactive)
      (cond ((file-exists-p (format "%sreadme.org" (projectile-project-root)))
             (find-file-other-window (format "%sreadme.org"
                                             (projectile-project-root))))
            ((file-exists-p (format "%sREADME.org" (projectile-project-root)))
             (find-file-other-window (format "%sREADME.org"
                                             (projectile-project-root))))
            ((file-exists-p (format "%sreadme.md" (projectile-project-root)))
             (find-file-other-window (format "%sreadme.md"
                                             (projectile-project-root))))
            ((file-exists-p (format "%sREADME.md" (projectile-project-root)))
             (find-file-other-window (format "%sREADME.md"
                                             (projectile-project-root))))
            (t (find-file-other-window (format "%sREADME.org"
                                               (projectile-project-root))))))

    )
#+end_src

** Develop

Useful defuns:

- (projectile-project-root)
- (projectile-get-project-directories)
- (projectile-ignored-directories-rel)
- (projectile-ignored-files-rel)
- (projectile-process-current-project-files)
- (projectile-project-name)

(setq files (-mapcat 'projectile-dir-files
		     (projectile-get-project-directories)))

# +begin_src emacs-lisp
  (defun csb/project-todo ()
    (interactive)
    (when (projectile-project-p)
      (require 'org-projectile)
      (let ((file (format "%s/%s" (expand-file-name (projectile-project-name)
						    user-projects-directory)
			  "TODO.org")))
	(unless (file-exists-p file) (write-region "" "" file))
	(setq org-projectile:projects-file file)
	(org-projectile:capture-for-project (projectile-project-name) nil))))
  (add-to-list 'org-capture-templates (org-projectile:project-todo-entry))
  (define-key projectile-mode-map (kbd "C") #'csb/project-todo)
  (define-key projectile-mode-map (kbd "C") #'org-projectile:template-or-project)
# +end_src

** Features

Here's a list of the interactive Emacs Lisp functions, provided by projectile:

C-c p f    Display a list of all files in the project. With a prefix argument it will clear the cache first.
C-c p d    Display a list of all directories in the project. With a prefix argument it will clear the cache first.
C-c p T    Display a list of all test files(specs, features, etc) in the project.
C-c p l    Display a list of all files in a directory (that's not necessarily a project)
C-c p g    Run grep on the files in the project.
C-c p b    Display a list of all project buffers currently open.
C-c p o    Runs multi-occur on all project buffers currently open.
C-c p r    Runs interactive query-replace on all files in the projects.
C-c p i    Invalidates the project cache (if existing).
C-c p R    Regenerates the projects TAGS file.
C-c p k    Kills all project buffers.
C-c p D    Opens the root of the project in dired.
C-c p e    Shows a list of recently visited project files.
C-c p a    Runs ack on the project. Requires the presence of ack-and-a-half.
C-c p c    Runs a standard compilation command for your type of project.
C-c p p    Runs a standard test command for your type of project.
C-c p z    Adds the currently visited to the cache.
C-c p s    Display a list of known projects you can switch to.

** References

 - http://endlessparentheses.com/improving-projectile-with-extra-commands.html?source=rss
 - [[http://batsov.com/projectile/][Web Site @ GitHub]]
 - <<exploring>> [[https://tuhdo.github.io/helm-projectile.html][Exploring large projects with Projectile and Helm Projectile]]
 - [[mu4e:msgid:yu1sh1ld3mo.fsf@apc.univ-paris7.fr][Re: Production tests JEUDI 4 Oct. 11h]]

* Helm-projectile

Activate, see [[exploring][this]] article.

#+begin_src emacs-lisp
  (use-package helm-projectile
    :init (helm-projectile-on)
    :config
    (setq
     helm-projectile-fuzzy-match t
     helm-projectile-sources-list '(helm-source-projectile-recentf-list
                                    helm-source-projectile-buffers-list
                                    helm-source-projectile-files-list
                                    helm-source-projectile-projects)))
#+end_src

Displaying all sources in helm-projectile can be slow. In that case, you should only use two
sources: helm-source-projectile-projects and helm-source-projectile-files-list. The first display a
list of projects and the second displays the current files in the selected project.

* Helm perso wiki

See [[file:csb-helm.page::*Hpw][Hpw]].

* Personal Workflow

** Config file

[[file:my_projects.el::%3B%3B%20Global%20purpose%20functions%20to%20be%20used%20in%20project%20creation%20and%20manipulation%20!!!][config]] common file, required by the emacs' init routine .emacs. This file includes common global variables to all
projects,  as well as an autoload procedure for calling the project named NAME from within emacs (M-x NAME).

** Project files for 'prj' project NAME

NAME folder :: Each project NAME appears in a folder

+ Project Files

  - .dir-locals.el :: per-directory local variables, see [[info:emacs#Directory%20Variables][info]] pages for more on this topic

  - NAME.el :: project configuration file, which sets up the emacs environment for this project; this file must
	       include a "NAME" defun

  - NAME.org :: file within the NAME folder, detailing the project, organization, folder structure, etc.

  - .NAME.el :: configuration file for the NAME.org file

+ ARTICLE folder

  - NAME_article.org :: article org file

  - .NAME_article.el :: NAME_article.org configuration file

+ PRESENTATION folder

  - NAME_presentation.org :: presentation org file

  - .NAME_presentation.el :: NAME_presentation.org configuration file

** Variable and constant declaration

#+begin_src emacs-lisp :tangle no

  ;; current project name
  (defvar prj-current-name "template")

  ;; current project home dir
  (defvar prj-current-home-dir "~/Documents/OrgProjects/template")

  ;; list of valid projects
  (defconst prj-list-of-projects '("positioner" "small_daq" "template"))

  ;; ;; list of screens
  ;; (defvar prj-screens-list nil)

#+end_src

** Launcher function

#+begin_src emacs-lisp :tangle no

  (defun prj-launcher(prj-name)
    "Launcher of projects"
    ;; get project
    (interactive "sEnter project name ... : ")
    ;; set current project name
    (setq prj-current-name (concat prj-name "/"))
    ;; set current project home dir
    (cond
     ((string= prj-name "positioner")
      (setq prj-current-home-dir "~/Documents/ciemat/")
      (setq prj-current-name prj-name)
      (message "Project positioner found @ %s" prj-current-home-dir)
      )
     ((string= prj-name "small_daq")
      (setq prj-current-home-dir "~/")
      (setq prj-current-name prj-name)
      (message "Project positioner found @ %s" prj-current-home-dir)
      )
     (t
      (message "Unknown project. Choose one of %s" prj-list-of-projects)
      )
     )
    ;; load project file
    (load-file (concat prj-current-home-dir prj-current-name "/" prj-current-name ".el"))
    ;; execute project configuration
    (call-interactively (intern prj-current-name))
    )

#+end_src

** Helper functions

#+begin_src emacs-lisp :tangle no

  (defun prj-new-tab(NAME CREATE)
    (if CREATE
        (elscreen-create)            ;; create new tab
      )
    (elscreen-screen-nickname NAME)  ;; rename screen
    )

  (defun prj-buffer-exists (NAME)
    "Check if buffer already open."
    (not (eq nil (get-buffer NAME))))

  (defun prj-OpenBuffer(FILE NAME TOTO)
    "Opens a buffer FILE and renames it to NAME.
  Checks if buffer already exists by its name.
  The file appears at 'org_projects_home_dir/project-name/file'"
    (if (not (prj-buffer-exists NAME))
        (progn
          (find-file (concat prj-current-home-dir prj-current-name "/" FILE))
          (rename-buffer NAME)
          )
      (message (concat NAME " already exist"))
      )
    )

  ;; (defun OpenBufferAbsolute(file name)
  ;;   "Opens a buffer FILE and renames it to NAME.
  ;; Checks if buffer already exists by its name.
  ;; The file appears at 'org_projects_home_dir/project-name/file'"
  ;;   (if (not (buffer-exists name))
  ;;       (progn
  ;;         (find-file file)
  ;;         (rename-buffer name)
  ;;         )
  ;;     (message (concat name " already exist"))
  ;;     )
  ;;   )

  (defun prj-OpenBufferDirex(FILE NAME)
    "Opens a buffer FILE and renames it to NAME.
  Checks if buffer already exists by its name.
  The file appears at 'org_projects_home_dir/project-name/file'"
    (if (not (buffer-exists NAME))
        (progn
          (direx:find-directory (concat org-projects-home-dir project-name FILE))
          (rename-buffer NAME)
          )
      (message (concat NAME " already exist"))
      )
    )

  (defun create-new-eshell(NUMBER NAME &optional COMMAND1 COMMAND2)
    (interactive)
    (if (not (buffer-exists NAME))
        (progn
          (eshell NUMBER)
          (rename-buffer NAME)
          (insert_command COMMAND1)
          (insert_command COMMAND2)
          )
      (message (concat NAME " already exist"))
      )
    )

#+end_src

* Trailer

#+begin_src emacs-lisp
  (provide 'csb-project-management)

  ;;; csb-project-management.el ends here
#+end_src

* References

 - http://www.devalot.com/articles/2008/07/project-planning
