;; vhdl-tools.el --- Utilities for navigating vhdl sources. -*- lexical-binding: t; -*-

(defvar csb/simple-config nil
  "flag to determine if simple config is to apply")

(setq package-user-dir "~/emacs-packages/elpa")

;; (package-initialize)

(defvar running-os-is-linux (eq system-type 'gnu/linux)
  "OS used as bootloader for emacs")

(defvar org-config-path (if running-os-is-linux
                            "~/Documents/PersonalWiki/wikidata/emacs.cat/org-config.cat/"
                          "~/Documents/PersonalWiki/wikidata/emacs.cat/org-config.cat/")
  "path to the org config files")

(setq byte-compile-warnings '(not free-vars))

(defvar org-load-path nil
  "path to search for org-file to load with org-require")
(add-to-list 'org-load-path org-config-path)

(require 'cl)

;; (defun org-require (orgfile &optional compile)
;;   "orgfile is a symbol to be loaded"
;;   (let* ((page-file
;; 	  ;; find the page-file
;; 	  (catch 'result
;; 	    (loop for dir in org-load-path do
;; 		  (when (file-exists-p
;; 			 (setq page-file (format "%s/%s.page" (directory-file-name dir) (symbol-name orgfile))))
;; 		    (throw 'result page-file)))))
;; 	 ;; org loads only .org files, not .page files !
;; 	 (org-file (format "%s.org" (file-name-sans-extension page-file)))
;; 	 (el-file (format "%s.el" (file-name-sans-extension page-file)))
;; 	 (elc-file (format "%s.elc" (file-name-sans-extension page-file))))
;;     ;; tangle and load only when
;;     ;; - no existing elfile
;;     ;; - modified page file
;;     ;; - no existing elcfile and compile requested
;;     (if (or (not (file-exists-p el-file))
;; 	    (file-newer-than-file-p page-file el-file)
;; 	    (and (not (file-exists-p elc-file))
;; 		 compile))
;; 	(progn
;; 	  ;; delete old orgfile if any
;; 	  (when (file-exists-p org-file)
;; 	    (delete-file org-file))
;; 	  ;; delte elc file when not compile required
;; 	  (when (and (not compile)
;; 		     (file-exists-p elc-file))
;; 	    (delete-file elc-file))
;; 	  ;; backup page into org file
;; 	  (copy-file page-file org-file)
;; 	  ;; tangle and load el file
;; 	  (org-babel-load-file org-file compile)
;; 	  ;; erase trailing orgfile
;; 	  (when (file-exists-p org-file)
;; 	    (delete-file org-file)))
;;       ;; just load elfile
;;       (load-file el-file))))

(defun org-require (orgfile &optional compile delay)
  "orgfile is a symbol to be loaded"
  (let* ((page-file
	  ;; find the page-file
	  (catch 'result
	    (loop for dir in org-load-path do
		  (when (file-exists-p
			 (setq page-file (format "%s/%s.page" (directory-file-name dir) (symbol-name orgfile))))
		    (throw 'result page-file)))))
	 ;; org loads only .org files, not .page files !
	 (org-file (format "%s.org" (file-name-sans-extension page-file)))
	 (el-file (format "%s.el" (file-name-sans-extension page-file)))
	 (elc-file (format "%s.elc" (file-name-sans-extension page-file))))
    ;; tangle and load only when
    ;; - no existing elfile
    ;; - modified page file
    ;; - no existing elcfile and compile requested
    (if (or (not (file-exists-p el-file))
	    (file-newer-than-file-p page-file el-file)
	    (and (not (file-exists-p elc-file))
		 compile))
	(progn
	  ;; delete old orgfile if any
	  (when (file-exists-p org-file)
	    (delete-file org-file))
	  ;; delte elc file when not compile required
	  (when (and (not compile)
		     (file-exists-p elc-file))
	    (delete-file elc-file))
	  ;; backup page into org file
	  (copy-file page-file org-file)
	  ;; tangle and load el file
	  (org-babel-load-file org-file compile)
	  ;; erase trailing orgfile
	  (when (file-exists-p org-file)
	    (delete-file org-file)))
      ;; just load elfile
      (if (and delay
               (string= (and (daemonp) (daemonp)) "default"))
	  (let ((tt (intern (make-temp-name "t"))))
	    (setq tt
		  (run-with-idle-timer delay 1
				       (lambda ()
					 (load-file el-file)
					 (cancel-timer tt)))))
	(load-file el-file)))))

;; (defun org-require (orgfile &optional compile)
;;   "orgfile is a symbol to be loaded"
;;   (let ((org-file (concat (symbol-name orgfile) ".page"))
;;         (path))
;;     ;; find the org-file
;;     (catch 'result
;;       (loop for dir in org-load-path do
;;             (when (file-exists-p
;;                    (setq path
;;                          (concat
;;                           (directory-file-name dir)
;;                           "/"
;;                           org-file)))
;;               (throw 'result path))))
;;     ;; org loads only .org files, not .page files !
;;     (let ((path-new (concat (file-name-sans-extension path) ".org")))
;;       (when (file-exists-p path-new)
;;         (delete-file path-new))
;;       (copy-file path path-new)
;;       (org-babel-load-file path-new compile)
;;       (when (file-exists-p path-new)
;;         (delete-file path-new)))))

(defvar csb/compile-org-require nil "compile org-required files")

(setq custom-file "~/.emacs.d/emacs-custom")

(package-initialize)

(when (not (fboundp 'make-variable-frame-local))
  (defun make-variable-frame-local (variable) variable))

(org-require 'csb-config)

(load custom-file 'noerror)

(put 'downcase-region 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)
