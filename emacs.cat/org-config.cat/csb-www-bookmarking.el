;;; csb-www-bookmarking --- Summary
;;
;;; Commentary:
;;
;; This file contains ...
;;
;;; Code:

(eval-when-compile
  (require 'org-protocol)
  (require 'org-attach))

(defvar csb/bookmarking-string
"String to ..."
"* %:description  %^g
    :PROPERTIES:
    :Private: %^{private|Yes|No}
    :ReadLater: %^{read later|Yes|No}
    :Effort: %^{effort|1:00|0:05|0:15|0:30|2:00|4:00|5:00}
    :END:\n\nDate:  %u  \\\\\nID:    [[file:/bookmarks.cat/%(substring (number-to-string (abs (/ (random) 100000000))) 0 6)][link]] \\\\\nTitle: %c \\\\\nURL:   %:link \\\\\n\nDescription:  %i\n\n%?")

(defvar csb/bookmarking-dir
  "docstring" nil)

(with-eval-after-load 'eww
  (add-to-list 'org-capture-templates
               `("b" "eww bookmarks" entry
                 (file
                  (let* ((_dir_ (format "%s%s/bookmarks.cat"
                                         (car (split-string default-directory
                                                            (persp-name persp-curr)))
                                         (persp-name persp-curr)))
                         (_file_ (format "%s/bookmarks.page" _dir_)))
                    ;; set here the place where to store the attachment and
                    ;; ID - see hook section below
                    (setq csb/bookmarking-dir _dir_)
                    _file_))
                 ,csb/bookmarking-string
                 :empty-lines 1)))

(with-eval-after-load 'eww
  (add-to-list 'org-capture-templates-contexts '("b" ((in-mode . "eww-mode")))))

(with-eval-after-load 'eww
  (define-key eww-mode-map "c"
    (lambda()(interactive)
      (if (not (and (boundp helm-perso-wiki-mode)
                    helm-perso-wiki-mode
                    (file-exists-p (format "%s%s/bookmarks.cat/index.page"
                                           (car (split-string default-directory
                                                              (persp-name persp-curr)))
                                           (persp-name persp-curr)))
                    (file-exists-p (format "%s%s/bookmarks.cat/bookmarks.page"
                                           (car (split-string default-directory
                                                              (persp-name persp-curr)))
                                           (persp-name persp-curr)))))
          (message "Sorry, don't know how to bookmark.
Create wiki project or category in already in a wiki.")
        (progn
          (require 'org-protocol)
          (if (ad-is-advised 'org-protocol-capture)
              (ad-deactivate 'org-protocol-capture))
          (if (ad-is-advised 'org-capture-finalize)
              (ad-deactivate 'org-capture-finalize))
          (if (ad-is-advised 'org-capture-kill)
              (ad-deactivate 'org-capture-kill))
          (setq-local org-protocol-data-separator "--")
          (org-protocol-capture
           (format "b--%s--%s--%s"
                   (plist-get eww-data :url)
                     (plist-get eww-data :title)
                   (if (and (region-active-p) (use-region-p))
                       (buffer-substring-no-properties (region-beginning)
                                                       (region-end))
                     "Enter desc here")))
          (ad-activate 'org-capture-kill)
          (ad-activate 'org-capture-finalize)
          (ad-activate 'org-protocol-capture))))))

(stante-after which-key
              (add-to-list 'which-key-allow-regexps "C-x j"))

(setq bookmark-save-flag t)

(setq bookmarnit+ak-default-file '"~/.emacs.d/.bookmarks")
(setq bmkp-last-as-first-bookmark-file "~/.emacs.d/.bookmarks")
(setq bookmark-default-file "~/.emacs.d/.bookmarks")

(with-eval-after-load 'bookmark
  ;;(define-key bookmark-map (kbd "h") #'helm-bookmarks)
)

(add-hook 'bookmark-bmenu-mode-hook
          (lambda ()
            (define-key bookmark-bmenu-mode-map (kbd "Q") 'kill-this-buffer)))

(with-eval-after-load 'bookmark
  (require 'bookmark+))

(provide 'csb-www-bookmarking)

;;; csb-www-bookmarking.el ends here
