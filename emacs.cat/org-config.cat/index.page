---
format: Org
categories: emacs org-config
toc: yes
title: Index of Emacs Configuration
content: Emacs Configuration index file
...

This category contains my emacs configuration files.

Booting emacs is a two-stage process.

- First, read the =init.el= file under =$HOME/.emacs.d.=

  This file contains basic setup. I get it by manually tangling the [[file:/emacs.cat/org-config.cat/csb-init][csb-init]] file.
  At the end of this file, I =org-require= the full configuration package file
  [[file:/emacs.cat/org-config.cat/csb-config][csb-config]], as depicted [[http://kitchingroup.cheme.cmu.edu/blog/2014/06/24/Using-org-files-like-el-files/][here]].

- The =csb-config= file is automatically loaded by =org-babel=, which tangles its contents to an
  elisp file.

- I recursively repeat the same process: the =csb-config= file contains another =csb-topic= files,
  which are =org-required=. They will be automatically tangled to the corresponding =elisp= file.

* Table of Contents
:PROPERTIES:
:VISIBILITY: content
:END:

[[file:/emacs.cat/org-config.cat/csb-apps][csb-apps]]	 :: csb-apps, content:

[[file:/emacs.cat/org-config.cat/csb-backup][csb-backup]]	 :: csb-backup, emacs backup config file

[[file:/emacs.cat/org-config.cat/csb-c-mode][csb-c-mode]]	 :: csb-c-mode, emacs c-mode mode config file

[[file:/emacs.cat/org-config.cat/csb-calendar][csb-calendar]]	 :: csb-calendar, emacs calendar config file

[[file:/emacs.cat/org-config.cat/csb-completions][csb-completions]]	 :: csb-completions, emacs completions config file

[[file:/emacs.cat/org-config.cat/csb-config][csb-config]]	 :: csb-completions, emacs full config file

[[file:/emacs.cat/org-config.cat/csb-debug][csb-debug]]	 :: csb-debug, emacs debug config file

[[file:/emacs.cat/org-config.cat/csb-deft][csb-deft]]	 :: csb-deft, emacs deft config file

[[file:/emacs.cat/org-config.cat/csb-dired][csb-dired]]	 :: csb-dired, emacs dired mode config file

[[file:/emacs.cat/org-config.cat/csb-docview][csb-docview]]	 :: csb-docview, emacs config doc

[[file:/emacs.cat/org-config.cat/csb-elfeed][csb-elfeed]]	 :: csb-elfeed, emacs elfeed mode config file

[[file:/emacs.cat/org-config.cat/csb-elisp][csb-elisp]]	 :: csb-elisp, emacs elisp mode config file

[[file:/emacs.cat/org-config.cat/csb-fringes][csb-fringes]]	 :: csb-fringes, emacs config fringes

[[file:/emacs.cat/org-config.cat/csb-global-togglemap][csb-global-togglemap]]	 :: csb-global-launchermap, implements a global toggle for emacs apps

[[file:/emacs.cat/org-config.cat/csb-helm][csb-helm]]	 :: csb-helm, emacs config helm

[[file:/emacs.cat/org-config.cat/csb-help][csb-help]]	 :: csb-help, emacs help mode config file

[[file:/emacs.cat/org-config.cat/csb-init][csb-init]]	 :: init.el, Emacs configuration init.el file

[[file:/emacs.cat/org-config.cat/csb-interactive-shell][csb-interactive-shell]]	 :: csb-interactive-shell, emacs interactive-shell mode config file

[[file:/emacs.cat/org-config.cat/csb-irc][csb-irc]]	 :: csb-irc, emacs irc mode config file

[[file:/emacs.cat/org-config.cat/csb-julia][csb-julia]]	 :: csb-julia, emacs julia mode config file

[[file:/emacs.cat/org-config.cat/csb-matlab][csb-matlab]]	 :: csb-matlab, emacs matlab mode config file

[[file:/emacs.cat/org-config.cat/csb-minimal][csb-minimal]]	 :: csb-minimal, emacs minimal mode config file

[[file:/emacs.cat/org-config.cat/csb-miscellaneous][csb-miscellaneous]]	 :: csb-miscellaneous, miscellaneous emacs functions

[[file:/emacs.cat/org-config.cat/csb-modal][csb-modal]]	 :: csb-modal, emacs modal config file

[[file:/emacs.cat/org-config.cat/csb-mu4e][csb-mu4e]]	 :: csb-mu4e, emacs mail config file

[[file:/emacs.cat/org-config.cat/csb-neotree][csb-neotree]]	 :: csb-octave, emacs neotree mode config file

[[file:/emacs.cat/org-config.cat/csb-octave][csb-octave]]	 :: csb-octave, emacs octave mode config file

[[file:/emacs.cat/org-config.cat/csb-orgmode-capture][csb-orgmode-capture]]	 :: csb orgmode/capture, emacs orgmode setup

[[file:/emacs.cat/org-config.cat/csb-orgmode][csb-orgmode]]	 :: csb orgmode, emacs orgmode setup

[[file:/emacs.cat/org-config.cat/csb-outshine][csb-outshine]]	 :: csb-outshine, emacs outshine /navy / outorg mode config file

[[file:/emacs.cat/org-config.cat/csb-pandoc][csb-pandoc]]	 :: csb-pandoc, emacs pandoc mode config file

[[file:/emacs.cat/org-config.cat/csb-pdf][csb-pdf]]	 :: csb-pdf, emacs config pdf

[[file:/emacs.cat/org-config.cat/csb-project-management-ggtags][csb-project-management-ggtags]]	 :: title:, content:

[[file:/emacs.cat/org-config.cat/csb-project-management][csb-project-management]]	 :: csb-project-management, emacs config for project management

[[file:/emacs.cat/org-config.cat/csb-python][csb-python]]	 :: csb-python, emacs python mode config file

[[file:/emacs.cat/org-config.cat/csb-references][csb-references]]	 :: csb orgmode/references, emacs orgmode setup

[[file:/emacs.cat/org-config.cat/csb-repositories][csb-repositories]]	 :: csb-repositories, emacs config repository

[[file:/emacs.cat/org-config.cat/csb-search][csb-search]]	 :: csb-search, emacs search config file

[[file:/emacs.cat/org-config.cat/csb-spell][csb-spell]]	 :: csb-spell, emacs spell mode config file

[[file:/emacs.cat/org-config.cat/csb-terminal][csb-terminal]]	 :: csb-terminal, emacs config terminal

[[file:/emacs.cat/org-config.cat/csb-vc][csb-vc]]	 :: csb-vc, emacs vc mode config file

[[file:/emacs.cat/org-config.cat/csb-vhdl][csb-vhdl]]	 :: csb-vhdl, emacs vhdl mode config file

[[file:/emacs.cat/org-config.cat/csb-windows][csb-windows]]	 :: csb-windows, emacs config windows

[[file:/emacs.cat/org-config.cat/csb-www-bookmarking][csb-www-bookmarking]]	 :: WWW bookmarking, How I implement bookmarking

[[file:/emacs.cat/org-config.cat/csb-www][csb-www]]	 :: csb-www, eww, w3m and similar setup

[[file:/emacs.cat/org-config.cat/sx][sx]]	 :: csb-sx, emacs sx mode config file

----------
