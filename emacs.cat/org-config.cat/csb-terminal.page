---
format: Org
categories:
toc: yes
title: csb-terminal
content: emacs config terminal
...

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[#introduction][Introduction]]
- [[#header][Header]]
- [[#introduction-1][Introduction]]
- [[#commint-mode][Commint mode]]
- [[#variables][Variables]]
- [[#generic][Generic]]
  - [[#toggle][Toggle]]
- [[#eshell][EShell]]
  - [[#variables-1][Variables]]
  - [[#aliases][Aliases]]
  - [[#helm-eshell][Helm Eshell]]
  - [[#drop-down][Drop-down]]
  - [[#toggle-1][Toggle]]
  - [[#eshell-git][Eshell git]]
  - [[#eshell-in-shell][Eshell in shell]]
  - [[#keys][Keys]]
  - [[#autosuggestions][Autosuggestions]]
  - [[#plan-9][Plan 9]]
  - [[#commands][Commands]]
  - [[#hooks][Hooks]]
  - [[#_references_][_References_]]
- [[#shell][Shell]]
- [[#ansi-term][Ansi-term]]
  - [[#toggle-2][Toggle]]
- [[#term-mode][Term mode]]
- [[#multi-term][Multi term]]
- [[#trailer][Trailer]]

* Introduction

This is the configuration file corresponding to emacs octave-mode.

* Header

#+begin_src emacs-lisp
;; package --- Summary
;;
;;; Commentary:
;;
;; This file contains ...
;;
;;; Code:
#+end_src

* Introduction

Code management of several different types of terminals inside emacs, including
inferior modes al eshell, ansi-term, python, matlab, etc.

#+begin_src emacs-lisp
  (eval-when-compile
    (require 'eshell))
#+end_src

* Commint mode

#+begin_src emacs-lisp
  (add-hook 'comint-mode-hook #'(lambda ()
                                  (setq-local show-trailing-whitespace nil)))
#+end_src

* Variables

These avoid problems with =projectile= and =ggtags= which pop out when setting
them to =/bin/zsh=. I ignore why.

#+begin_src emacs-lisp
  (when (executable-find "zsh")
    ;; (setq explicit-shell-file-name "/usr/bin/zsh")
    (setq explicit-shell-file-name "/bin/sh")
    ;; (setq shell-file-name "/usr/bin/zsh")
    (setq shell-file-name "/bin/sh")
    (setenv "SHELL" (getenv "SHELL")))
  ;; (setq explicit-bash.exe-args '("--noediting" "--login" "-i"))
#+end_src

(setenv "SHELL" "/usr/sbin/bash")
(getenv "SHELL")

* Generic

Valid for all kinds of terms.

** Toggle

TODO: compare with =shell-pop=.

Toggle show terminal

- create a new ansi-term if none exist (in current persp); otherwise, switch to it
- cd to current dir
- open window at the bottom, one third of the current window
- with a prefix argument, open other window to the right; C-u C-u, full screen
- if already on an ansi-term, switch back to previous window configuration.

#+begin_src emacs-lisp
  (defmacro csb/toggle-terminals(term-type)
    `(defun ,(intern (format "csb/term-toggle-%s" term-type)) (arg)
       (interactive "P")
       (if (string=
            (cond ((string= ,term-type "ansi-term")
                   "term-mode")
                  ((string= ,term-type "julia")
                   "inferior-ess-mode")
                  ((string= ,term-type "Python")
                   "inferior-python-mode")
                  ((string= ,term-type "MATLAB")
                   "matlab-shell-mode")
                  ((string= ,term-type "eshell")
                   (format "%s-mode" ,term-type))
                  (t ""))
            major-mode)
           ;; recover window config
           (jump-to-register
            (intern (format "csb/toggle-terms-register-%s-%s"
                            (persp-name (persp-curr))
                            ,term-type)))
         ;; else
         (let* (;; name of the terminal
                (term-name
                 (cond
                  ;; append project root in the case of julia
                  ((string= ,term-type "julia")
                   (format "*%s:%s*" ,term-type
                           (format "%s" (file-name-base
                                         (directory-file-name
                                          (vc-find-root
                                           (or (buffer-file-name) default-directory)
                                           ".git"))))))
                  ((string= ,term-type "ansi-term")
                   (format "*%s*" ,term-type))
                  ((string= ,term-type "Python")
                   (format "*%s*" ,term-type))
                  ((string= ,term-type "MATLAB")
                   (format "*%s*" ,term-type))
                  ((string= ,term-type "eshell")
                   (format "*%s:%s*" ,term-type
                           (format "%s" (file-name-base
                                         (directory-file-name
                                          (vc-find-root
                                           (or (buffer-file-name) default-directory)
                                           ".git")))))
                   ;; (format "*%s*" ,term-type)
                   )
                  (t (format "*%s*" ,term-type))))
                (eshell-buffer-name term-name)
                (ansi-term-buffer-name
                 (format "%s:%s" ,term-type
                         (if ,(string= term-type "julia")
                             (format "%s" (file-name-base
                                           (directory-file-name
                                            (vc-find-root
                                             (or (buffer-file-name) default-directory)
                                             ".git"))))
                           ;; otherwise, persp name
                           (format "%s"
                                   (persp-name (persp-curr))))))
                (height (/ (window-total-height) 3))
                (width (/ (window-total-width) 2))
                (parent (if (buffer-file-name)
                            (file-name-directory (buffer-file-name))
                          default-directory)))
           (if ;; check if an eshell already open in current persp
               (let ((term-exists))
                 (catch 'error
                   (dolist (thisbuffer (persp-buffers (persp-curr)))
                     (when (and (buffer-name thisbuffer)
                                (string-prefix-p term-name (buffer-name thisbuffer)))
                       (setq term-exists t)
                       (throw 'error t))))
                 term-exists)
               (if
                   ;; exists and visible
                   (get-buffer-window term-name)
                   (select-window (get-buffer-window term-name))
                 ;; exists but not visible
                 (progn
                   ;; store current window config
                   (window-configuration-to-register
                    (intern (format "csb/toggle-terms-register-%s-%s"
                                    (persp-name (persp-curr))
                                    ,term-type)))
                   (cond ((equal arg '(4))
                          (split-window-horizontally (- width)))
                         ((equal arg '(16))
                          (delete-other-windows))
                         (t
                          (split-window-vertically (- height))))
                   (other-window 1)
                   (switch-to-buffer term-name)))
             ;; doesn't exists
             (progn
               ;; store current window config
               (window-configuration-to-register
                (intern (format "csb/toggle-terms-register-%s-%s"
                                (persp-name (persp-curr))
                                ,term-type)))
               (cond ((equal arg '(4))
                      (split-window-horizontally (- width)))
                     ((equal arg '(16))
                      (delete-other-windows))
                     (t
                      (split-window-vertically (- height))))
               (other-window 1)
               (cond ((string= ,term-type "ansi-term")
                      (ansi-term shell-file-name ansi-term-buffer-name))
                     ((string= ,term-type "julia")
                      (julia))
                     ((string= ,term-type "Python")
                      (let ((python-shell-interpreter "jupyter")
                            (python-shell-interpreter-args "console --simple-prompt"))
                        (run-python))
                      (switch-to-buffer (get-buffer (format "*%s*" ,term-type))))
                     ((string= ,term-type "MATLAB")
                      (matlab-shell))
                     ((string= ,term-type "eshell")
                      (eshell)
                      ;; (let ((eshell-buffer-name "*eshell*"))
                      ;;   )
                      )
                     (t ""))))
           ;; cd to current dir
           (cond ((string= ,term-type "ansi-term")
                  (term-send-raw-string (format "cd %s \n" parent)))
                 ((string= ,term-type "MATLAB")
                  (matlab-shell-send-string (format "cd %s \n" parent)))
                 ((string= ,term-type "eshell")
                  (insert (format "cd %s" parent))
                  (eshell-send-input))
                 (t nil))))))
#+end_src

* EShell

** Variables

Must be in agreement with [[file:init.el::%3B%3B%20Environment][environment]] variables.

#+begin_src emacs-lisp
  ;; (setq eshell-login-script (concat user-emacs-directory "eshell/login"))
  ;; (setq eshell-rc-script (concat user-emacs-directory "eshell/profile"))
  ;; (setq explicit-bash.exe-args '("--noediting" "--login" "-i"))))
  (stante-after eshell
                (setq eshell-buffer-name "*eshell*"
                      eshell-scroll-show-maximum-output nil
                      eshell-prefer-lisp-functions t))
#+end_src

** Aliases

Set aliases file: copy zsh aliases and fix it to eshell format

#+begin_src emacs-lisp :tangle no
  (stante-after eshell

                (setq eshell-aliases-file (make-temp-file (format "eshell-alias-")))

                (defun csb/eshell-update-aliases()
                  "Copy zsh aliases file to eshell and perform modifs to make it usable"
                  (message "updating eshell alias from zsh")
                  ;; get file
                  (copy-file "~/DotFiles/zsh/aliases.zsh" eshell-aliases-file t t t)
                  ;; fix aliases
                  (with-current-buffer (find-file-noselect eshell-aliases-file)
                    ;; comment out this line to avoid shading eshell/ls
                    (goto-char (point-min))
                    (while (re-search-forward "alias ls='ls" nil t)
                      (replace-match "# alias ls='ls"))
                    ;; chage string "='" for " "
                    (goto-char (point-min))
                    (while (re-search-forward "=’" nil t)
                      (replace-match " "))
                    ;; chage string "'" for " "
                    (goto-char (point-min))
                    (while (re-search-forward "'" nil t)
                      (replace-match " "))
                    (save-buffer)
                    (kill-this-buffer)))

                (when (string= (system-name) "apcnb158")
                  (csb/eshell-update-aliases)))
#+end_src

** Helm Eshell

[[https://tuhdo.github.io/helm-intro.html#sec-27][Helm]] and eshell to complete history

#+begin_src emacs-lisp
  (stante-after eshell
                (add-hook 'eshell-mode-hook
                          #'(lambda ()
                              ;; (eshell-cmpl-initialize)
                              (define-key eshell-mode-map [remap eshell-pcomplete] 'helm-esh-pcomplete)
                              (define-key eshell-mode-map (kbd "C-x C-h") 'helm-eshell-history))))
#+end_src

** Drop-down

1. I can toggle a shell full screen, saving window config.
   Based on [[http://irreal.org/blog/?p%3D1742][A Full Screen EShell]].

 #+begin_src emacs-lisp :tangle no
   (defun toggle-shell-full-screen ()
     "Bring up a full-screen eshell or restore previous config."
     (interactive)
     (if (string= "eshell-mode" major-mode)
         (jump-to-register :eshell-fullscreen)
       (progn
         (window-configuration-to-register :eshell-fullscreen)
         (eshell)
         (delete-other-windows))))
 #+end_src

2. Drop-down Shell based on [[https://tsdh.wordpress.com/2011/10/12/a-quick-pop-up-shell-for-emacs/][this]] article. Opens eshell in current dir.

 #+begin_src emacs-lisp :tangle no
   (defvar th-eshell-popup-buffer nil)

   (defun th-shell-popup ()
     "Toggle a shell popup buffer with the current file's directory as cwd."
     (interactive)
     (unless (buffer-live-p th-eshell-popup-buffer)
       (save-window-excursion (eshell))
       (setq th-eshell-popup-buffer (get-buffer "*eshell*")))
     (let ((win (get-buffer-window th-eshell-popup-buffer))
           (dir (file-name-directory (or (buffer-file-name)
                                         dired-directory ;; dired
                                         "~/"))))  ;; use HOME
       (if win
           (quit-window nil win)
         (pop-to-buffer th-eshell-popup-buffer nil t)
         (insert (concat "cd " dir))
         (eshell-send-input))))
   ;;(comint-send-string nil (concat "cd " dir "\n"))))
 #+end_src

3. Pop out shell, based on [[http://www.howardism.org/Technical/Emacs/eshell-fun.html][this]] article

#+begin_src emacs-lisp :tangle no
  (defun eshell-here ()
    "Opens up a new shell in the directory associated with the
      current buffer's file. The eshell is renamed to match that
      directory to make multiple eshell windows easier."
    (interactive)
    (let* ((parent (if (buffer-file-name)
                       (file-name-directory (buffer-file-name))
                     default-directory))
           (height (/ (window-total-height) 3))
           (name   (car (last (split-string parent "/" t)))))
      (split-window-vertically (- height))
      (other-window 1)
      (eshell "new")
      (rename-buffer (concat "*eshell: " name "*"))
      (insert (concat "ls"))
      (eshell-send-input)))
#+end_src

** Toggle

#+begin_src emacs-lisp
  (csb/toggle-terminals "eshell")
#+end_src

** Eshell git

#+begin_src emacs-lisp
  (stante-after eshell
                (eshell-git-prompt-use-theme 'robbyrussell))
#+end_src

** Eshell in shell

#+begin_src emacs-lisp
  (defun csb/my-eshell-frame (directory)
    "Open up a dired frame which closes on exit."
    (switch-to-buffer (eshell directory))
    (local-set-key
     (kbd "C-x C-c")
     (lambda ()
       (interactive)
       (insert (concat "exit"))
       (eshell-send-input)
       (save-buffers-kill-terminal 't))))
#+end_src

** Keys

#+begin_src emacs-lisp
  (stante-after eshell
                (add-hook 'eshell-mode-hook
                          #'(lambda ()
                              (define-key eshell-mode-map (kbd "C-j") 'eshell-send-input))))
#+end_src

** Autosuggestions

[[http://whyarethingsthewaytheyare.com/fishlike-autosuggestions-in-eshell/]]

#+begin_src emacs-lisp :tangle no
  (defun eshell-autosuggest-candidates (prefix)
    (let* ((history
            (delete-dups
             (mapcar (lambda (str)
                       (string-trim (substring-no-properties str)))
                     (ring-elements eshell-history-ring))))
           (most-similar (cl-find-if
                          (lambda (str)
                            (string-prefix-p prefix str))
                          history)))
      (when most-similar
        `(,most-similar))))

  (defun eshell-autosuggest--prefix ()
    (let ((prefix
           (string-trim-left
            (buffer-substring-no-properties
             (save-excursion
               (eshell-bol))
             (save-excursion (end-of-line) (point))))))
      (if (not (string-empty-p prefix))
          prefix
        'stop)))

  (defun eshell-autosuggest (command &optional arg &rest ignored)
    (interactive (list 'interactive))
    (cl-case command
      (interactive (company-begin-backend 'company-eshell))
      (prefix (and (eq major-mode 'eshell-mode)
                   (eshell-autosuggest--prefix)))
      (candidates (eshell-autosuggest-candidates arg))))
#+end_src

#+begin_src emacs-lisp :tangle no
  (defun setup-eshell-autosuggest ()
    (with-eval-after-load 'company
      (setq-local company-backends '(eshell-autosuggest))
      (setq-local company-frontends '(company-preview-frontend))))
#+end_src

** Plan 9

#+begin_src emacs-lisp
  (stante-after eshell
                (require 'em-smart)
                (setq eshell-where-to-jump 'begin)
                (setq eshell-review-quick-commands nil)
                (setq eshell-smart-space-goes-to-end t))
#+end_src

** Commands

#+begin_src emacs-lisp
  (stante-after eshell
                (defun eshell/e (&rest args)
                  (insert "exit")
                  (eshell-send-input)
                  ;; (unless (< (count-windows ) 2)
                  ;;   (delete-window))
                  ))
#+end_src

#+begin_src emacs-lisp
  (stante-after eshell
                (defun eshell/q (&rest ARGS)
                  (bury-buffer)
                  (unless (< (count-windows ) 2)
                    (delete-window))))
#+end_src

** Hooks

#+begin_src emacs-lisp
  (stante-after eshell
                (add-hook 'eshell-mode-hook
                          #'(lambda ()
                              (require 'eshell-z)
                              (eshell-bookmark-setup)
                              (smartscan-mode -1)
                              (electric-indent-local-mode -1)
                              ;; (setup-eshell-autosuggest)
                              (company-mode -1)
                              ;; (hide-mode-line-mode t)
                              (add-to-list 'eshell-visual-commands "glances")
                              (ansi-color-for-comint-mode-on)
                              (setq-local show-trailing-whitespace nil))))
#+end_src

#+begin_src emacs-lisp :tangle no
  (defun oleh-term-exec-hook ()
    (let* ((buff (current-buffer))
           (proc (get-buffer-process buff)))
      (set-process-sentinel
       proc
       `(lambda (process event)
          (if (string= event "finished\n")
              (kill-buffer ,buff))))))

  (add-hook 'term-exec-hook 'oleh-term-exec-hook)
#+end_src

** _References_

 - [[file:~/Documents/OrgAgenda/bookmarks.org::*%5B%5Bhttp:/www.masteringemacs.org/article/complete-guide-mastering-eshell%5D%5BMasteringEmacs:Complete%20Guide%20to%20EShell%5D%5D][MasteringEmacs:Complete Guide to EShell]]
 - [[http://irreal.org/blog/?p=2756][Eshell and Abbreviations]]
 - [[http://www.howardism.org/Technical/Emacs/eshell-fun.html][Eschewing Zshell for Emacs Shell]]
 - https://www.youtube.com/watch?v=RhYNu6i_uY4
 - [[http://whyarethingsthewaytheyare.com/fishlike-autosuggestions-in-eshell/]]
 - [[elfeed:irreal.org#http://irreal.org/blog/?p=6711][Howard Abrams on Eshell]]
 - [[https://pinboard.in/u:csantosb/t:eshell/]]

* Shell

#+begin_src emacs-lisp
  (add-hook 'shell-mode-hook (lambda () (setq-local show-trailing-whitespace nil)))
#+end_src

* Ansi-term

** Toggle

#+begin_src emacs-lisp
  (csb/toggle-terminals "ansi-term")
#+end_src

* Term mode

#+begin_src emacs-lisp
  (add-hook 'term-mode-hook (lambda () (setq-local show-trailing-whitespace nil)))
#+end_src

* Multi term

#+begin_src emacs-lisp
;;(require 'multi-term)
;; (setq multi-term-program "/bin/zsh")
#+end_src

* Trailer

#+begin_src emacs-lisp
  (provide 'csb-terminal)

  ;;; csb-terminal.el ends here
#+end_src
