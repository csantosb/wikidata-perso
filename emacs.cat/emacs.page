---
format: Org
categories: emacs software
toc: yes
title: emacs
content: emacs tips
...

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[#notes][notes]]
- [[#eww][eww]]
- [[#build-emacs][Build emacs]]
  - [[#references][References]]
  - [[#build-script][Build Script]]
  - [[#install-script][Install Script]]
  - [[#pkgbuild][Pkgbuild]]
- [[#bookmarks-plus][bookmarks plus]]
  - [[#tutorial][Tutorial]]
  - [[#extending-the-command-set][EXTENDING THE COMMAND SET]]
  - [[#auto-save][AUTO SAVE]]
  - [[#echo-area][ECHO AREA]]
  - [[#mode-line][MODE LINE]]
  - [[#searching][SEARCHING]]
  - [[#multiple-windows][MULTIPLE WINDOWS]]
  - [[#multiple-frames][MULTIPLE FRAMES]]
  - [[#recursive-editing-levels][RECURSIVE EDITING LEVELS]]
  - [[#getting-more-help][GETTING MORE HELP]]
  - [[#more-features][MORE FEATURES]]
  - [[#conclusion][CONCLUSION]]
  - [[#copying][COPYING]]
- [[#modules][Modules]]

* notes

- as for [2015-01-09 Fri], helm-source is incompatible with current emacs devel, as
(eieio--class-slot-initarg) is the new name of class-slot-initarg, used by this package.

* eww

As for http://bit.ly/1BEeXNu, to fix a problem in eww with how titles are rendered, I comment out
this line in eww-render

;; (plist-put eww-data :title url)

* Build emacs

** References

http://www.masteringemacs.org/articles/2013/12/29/whats-new-in-emacs-24-4/
http://lars.ingebrigtsen.no/2014/11/13/welcome-new-emacs-developers/
http://esr.ibiblio.org/?p=6524#more-6524
http://git.savannah.gnu.org/cgit/emacs.git

** Build Script

Use

#+begin_src sh :tangle no
  tail -F emacs_build_make.log
#+end_src

to follow compile logs.

Then, to update this script [[elisp:(org-babel-tangle-publish%20nil%20"emacs.page"%20"~/Scripts")][tangle]] this file to $HOME/Scripts

#+begin_src sh :tangle emacs-build.sh
  #!/usr/bin/sh

  my_log=$HOME/emacs_build.log
  my_log_make=$HOME/emacs_build_make.log

  ## Help functions

  insert_trailer(){
      echo ""  | tee -a $my_log
      echo "Done." | tee -a $my_log
      echo "" >> $my_log_make
      echo "Done." >> $my_log_make
  }

  insert_log_header(){
      echo "" | tee -a $my_log
      echo "****"  | tee -a $my_log
      echo ""  | tee -a $my_log
      echo $1 | tee -a $my_log
  }

  insert_log_make_header(){
      echo "" | tee -a $my_log_make
      echo "****"  | tee -a $my_log_make
      echo ""  | tee -a $my_log_make
      echo $1 | tee -a $my_log_make
      echo ""  | tee -a $my_log_make
  }

  echo ""  | tee $my_log
  echo "NEW COMPILE"  | tee -a $my_log
  echo ""  | tee -a $my_log

  echo ""  | tee $my_log_make
  echo "NEW COMPILE"  | tee -a $my_log_make
  echo ""  | tee -a $my_log_make

  echo "****" | tee -a $my_log
  echo Inicio Emacs Build a las $(date) | tee -a $my_log

  # echo " "  >> $my_log
  # echo "****"  >> $my_log
  # echo Removing old version >> $my_log
  # [[ -d /programs/Programs_Original/emacs ]] && cd /programs/Programs_Original/emacs  >> $my_log
  # [[ -d /programs/Programs_Original/emacs ]] && sudo make uninstall >> $my_log


  ## Check sources update
  echo ""  | tee -a $my_log
  echo "****"  | tee -a $my_log
  echo ""  | tee -a $my_log
  echo "Updating sources ..." | tee -a $my_log
  echo ""  | tee -a $my_log
  if ([ -d /programs/Programs_Original/emacs ]) then
     cd /programs/Programs_Original/emacs
     # Fetch and merge sources
     result=$(git pull)
     [[ "$result" = "Already up-to-date." ]] && echo Sources up-to-date. Aborting compile. | tee -a $my_log ## && exit 1
  else
    cd /programs/Programs_Original
    echo "no sources found, clonning ..." | tee -a $my_log
    git clone git://git.sv.gnu.org/emacs.git | tee -a $my_log
    # git clone git://git.savannah.gnu.org/emacs.git
    echo "Done clonning."
  fi


     ## Cp sources to tmp dir
     insert_log_header "Copy repo to tmp dir ..."
     [[ -d /tmp/emacs ]] && rm -rf /tmp/emacs
     cp -r /programs/Programs_Original/emacs /tmp && cd /tmp/emacs
     insert_trailer


     ## Autogen
     insert_log_header "Rebuild emacs from source in tmp dir. Autogen ..."
     #
     insert_log_make_header "Autogen"
     ./autogen.sh | tee -a $my_log_make
     #
     insert_trailer


     ## Configure
     insert_log_header "Configuring ..."
     #
     insert_log_make_header "Configure"
     ./configure --prefix=/usr --with-x-toolkit=lucid >> $my_log_make # --with-x-toolkit=gtk3
     #
     insert_trailer


     ## Bootstrap
     insert_log_header "Bootstrapping ..."
     #
     insert_log_make_header "Bootstrap"
     make bootstrap -j 4 | tee -a $my_log_make
     #
     insert_trailer

     ## Make
     insert_log_header "Making ..."
     #
     insert_log_make_header "Make"
     make -j 4>> $my_log_make
     #
     insert_trailer


     ## Make Doc
     insert_log_header "Making Docs ..."
     #
     insert_log_make_header "Make Doc"
     make docs -j 4>> $my_log_make
     #
     insert_trailer

#+end_src

** Install Script

Use

#+begin_src sh :tangle no
  tail -F emacs_build_make.log
#+end_src

to follow compile logs.

Then, to update this script [[elisp:(org-babel-tangle-publish%20nil%20"emacs.page"%20"~/Scripts")][tangle]] this file to $/Scripts

#+begin_src sh :tangle emacs-install.sh

  #!/usr/bin/sh

  ## Uninstall old version
  [[ -d /programs/Programs_Original/emacs_compiled ]] && cd /programs/Programs_Original/emacs_compiled \
      && sudo make uninstall || { echo "no old compiled folder" && cd /tmp/emacs && sudo make uninstall}

  ## Install new version
  [[ -d /tmp/emacs ]] && cd /tmp/emacs && sudo make install || { echo "no compiled emacs found. Aborting." && exit 1 }

  ## Backup compiled version
  cd ~
  [[ -d /programs/Programs_Original/emacs_compiled ]] && sudo rm -rf /programs/Programs_Original/emacs_compiled
  mv /tmp/emacs /programs/Programs_Original/emacs_compiled

  # ## * Install libs
  # echo " "  >> $my_log
  # echo "****"  >> $my_log
  # echo " "  >> $my_log
  # echo rebuild libs >> $my_log
  # echo " "  >> $my_log
  # ~/Dropbox/scripts/build_emacs_libraries.sh >> $my_log

  # ## * Restart daemon
  # echo " "  >> $my_log
  # echo "****"  >> $my_log
  # echo " "  >> $my_log
  # echo Restart Daemon >> $my_log
  # echo " "  >> $my_log
  # sudo systemctl start emacs@csantos >> $my_log
  # sudo systemctl status emacs@csantos >> $my_log

  # echo Fin Emacs Build a las $(date) >> $my_log
  # echo Fin Emacs Build a las $(date) >> $my_log_make

#+end_src

** Pkgbuild

Using arch abs approach with [[https://aur.archlinux.org/packages/emacs-git/][this]] package.

Just =makepkg= to update to last release, auto update git repo.

#+begin_src sh :tangle no
#+end_src

* bookmarks plus

g - refresh

Create/Set Bookmarks (anywhere)
--------------------

C-x p RET	- Set/delete an autonamed bookmark here
C-x p c a	- Set and autoname a bookmark for a file
C-x p c f	- Set a bookmark for a file
C-x p c u	- Set a bookmark for a URL
C-x p m		- Set a bookmark here
C-x p K		- Set a bookmark for the current desktop
C-x p y		- Set a bookmark for a bookmark file


Jump to (Visit) Bookmarks
-------------------------

Here:

M-x bmkp-bmenu-jump-to-marked	- Bookmarks marked `>', in other windows
RET	- Bookmark in the same window
o	- Bookmark in another window
C-o	- Bookmark in other window, without selecting it
1	- Bookmark in a full-frame window
2	- This bookmark and last-visited bookmark

Anywhere (`C-x j C-h' to see this, and `C-x j t C-h' for tags):

C-x j j	- Bookmark by name
C-x j :	- Bookmark by type
C-x j N	- Bookmark in the navigation list
C-x j h	- Highlighted bookmark
C-x j a	- Autofile bookmark
C-x j #	- Autonamed bookmark
C-x j , #	- Autonamed bookmark in buffer
C-x j x	- Temporary bookmark
C-x j K	- Desktop bookmark
C-x j B	- Bookmark-list bookmark
C-x j y	- Bookmark-file bookmark
C-x j d	- Dired bookmark
C-x j f	- File or directory bookmark
C-x j . d	- Dired bookmark for this dir
C-x j . f	- Bookmark for a file or subdir in this dir
C-x j l	- Local-file bookmark
C-x j n	- Remote-file bookmark
C-x j r	- Region bookmark
C-x j M-i	- Image-file bookmark
C-x j i	- Info bookmark
C-x j m	- `man'-page bookmark
C-x j b	- Non-file (buffer) bookmark
C-x j g	- Gnus bookmark
C-x j u	- URL bookmark
C-x j v	- Variable-list bookmark

C-x j t +	- Bookmark having some tags you specify
C-x j t *	- Bookmark having each tag you specify
C-x j t % +	- Bookmark having a tag that matches a regexp
C-x j t % *	- Bookmark having all its tags match a regexp
C-x j t f +	- File bookmark having some tags you specify
C-x j t f *	- File bookmark having each tag you specify
C-x j t f % +	- File bookmark having a tag that matches a regexp
C-x j t f % *	- File bookmark having all its tags match a regexp
C-x j t . +	- File in this dir having some tags you specify
C-x j t . *	- File in this dir having each tag you specify
C-x j t . % +	- File in this dir having a tag that matches a regexp
C-x j t . % *	- File in this dir having all its tags match a regexp


Bookmark Tags
-------------

T +	- Add some tags to a bookmark
T -	- Remove some tags from a bookmark (`C-u': from all bookmarks)
T 0	- Remove all tags from a bookmark
T d	- Remove some tags from all bookmarks
T r	- Rename a tag in all bookmarks
T l	- List all tags used in any bookmarks (`C-u': show tag values)
T e	- Edit bookmark's tags
T v	- Set the value of a tag (as attribute)


** Tutorial

C-x 4  : other window

scroll-other-buffer M-RePag

HELP _____________________________________________________________________________________________

`C-h' or <F1> means "help" you can type them after a prefix key to view a list of the keys that can follow the prefix key

`C-h a TOPICS <RET>'       This searches for commands whose names match the argument TOPICS. The argument can be a keyword, a list of keywords, or a regular expression (*note Regexps::).  *Note Apropos::.

`C-h d TOPICS <RET>'       Display the commands and variables whose documentation matches TOPICS (`apropos-documentation').

INFO

`C-h r'       Display the Emacs manual in Info (`info-emacs-manual').

`C-h t'       Enter the Emacs interactive tutorial (`help-with-tutorial').

`C-h C-f'     This displays the Emacs FAQ, using Info.

`C-h i'       Run Info, the GNU documentation browser (`info').  The Emacs  manual is available in Info.

`C-h i d m emacs <RET> i TOPIC <RET>'
     This searches for TOPIC in the indices of the Emacs Info manual, displaying the first match found.  Press `,' to see subsequent matches.  You can use a regular expression as TOPIC.

`C-h i d m emacs <RET> s TOPIC <RET>'
     Similar, but searches the _text_ of the manual rather than the indices.

`C-h K KEY'         Enter Info and go to the node that documents the key sequence KEY (`Info-goto-emacs-key-command-node').

`C-h F COMMAND <RET>'	Enter Info and go to the node that documents the Emacs command COMMAND (`Info-goto-emacs-command-node').



`C-h p'       This displays the available Emacs packages based on keywords. *Note Package Keywords::.



`C-h c KEY'             Show the name of the command that the key sequence KEY is bound to (`describe-key-briefly').  Here `c' stands for "character".  For more extensive information on KEY, use `C-h k'.

`C-h k KEY              Display the name and documentation of the command that KEY runs (`describe-key').

`C-h w COMMAND <RET>'       Show which keys run the command named COMMAND (`where-is').


`C-h m'                 Display documentation of the current major mode (`describe-mode').

`C-h v VAR <RET>'       Display the documentation of the Lisp variable VAR (`describe-variable').

`C-h f FUNCTION <RET>'      Display documentation on the Lisp function named FUNCTION (`describe-function').  Since commands are Lisp functions, this works for commands too.


`C-h b'        Display all active key bindings; minor mode bindings first, then those of the major mode, then global bindings (`describe-bindings').

`C-h e'     Display the `*Messages*' buffer (`view-echo-area-messages').

`C-h h'		Display the `HELLO' file, which shows examples of various character sets.

`C-h l'		Display a description of your last 300 keystrokes (`view-lossage').

`C-h n'		Display news of recent Emacs changes (`view-emacs-news').

`C-h p'		Find packages by topic keyword (`finder-by-keyword').  This lists packages using a package menu buffer.  *Note Packages::.

`C-h P PACKAGE <RET>'         Display documentation about the package named PACKAGE (`describe-package').

`C-h s'
     Display the contents of the current "syntax table"
     (`describe-syntax').  The syntax table says which characters are
     opening delimiters, which are parts of words, and so on.  *Note
     Syntax Tables: (elisp)Syntax Tables, for details.

`C-h C CODING <RET>'          Describe the coding system CODING (`describe-coding-system').

`C-h C <RET>'             Describe the coding systems currently in use.

`C-h I METHOD <RET>'          Describe the input method METHOD (`describe-input-method').

`C-h L LANGUAGE-ENV <RET>'
     Display information on the character sets, coding systems, and
     input methods used in language environment LANGUAGE-ENV
     (`describe-language-environment').

`C-h S SYMBOL <RET>'
     Display the Info documentation on symbol SYMBOL according to the
     programming language you are editing (`info-lookup-symbol').

`C-h .'
     Display the help message for a special text area, if point is in
     one (`display-local-help').  (These include, for example, links in
     `*Help*' buffers.)


SPELL CHECK _______________________________________________________________________________

`DIGIT'
     Replace the word, just this time, with one of the displayed
     near-misses.  Each near-miss is listed with a digit; type that
     digit to select it.

`<SPC>'
     Skip this word--continue to consider it incorrect, but don't
     change it here.

`r NEW <RET>'
     Replace the word, just this time, with NEW.  (The replacement
     string will be rescanned for more spelling errors.)

`R NEW <RET>'
     Replace the word with NEW, and do a `query-replace' so you can
     replace it elsewhere in the buffer if you wish.  (The replacements
     will be rescanned for more spelling errors.)

`a'
     Accept the incorrect word--treat it as correct, but only in this
     editing session.

`A'
     Accept the incorrect word--treat it as correct, but only in this
     editing session and for this buffer.

`i'
     Insert this word in your private dictionary file so that Aspell or
     Ispell or Hunspell will consider it correct from now on, even in
     future sessions.

`m'
     Like `i', but you can also specify dictionary completion
     information.

`u'
     Insert the lower-case version of this word in your private
     dictionary file.

`l WORD <RET>'
     Look in the dictionary for words that match WORD.  These words
     become the new list of "near-misses"; you can select one of them as
     the replacement by typing a digit.  You can use `*' in WORD as a
     wildcard.

`C-g'
`X'
     Quit interactive spell checking, leaving point at the word that was
     being checked.  You can restart checking again afterward with `C-u
     M-$'.

`x'
     Quit interactive spell checking and move point back to where it was
     when you started spell checking.

`q'
     Quit interactive spell checking and kill the spell-checker
     subprocess.

`?'
     Show the list of options.






For instance, C-u 8 C-f moves forward eight characters.

F10       menu-bar-open
C-<spc>       select

Repeating a text character inserts it several times.
>>  Try that now -- type C-u 8 * to insert ********.

    <DEL>        Delete the character just before the cursor
    C-d          Delete the next character after the cursor

    M-<DEL>      Kill the word immediately before the cursor
    M-d      Kill the next word after the cursor

    C-k      Kill from the cursor position to end of line
    M-k      Kill to the end of the current sentence


What do you do if you have some text you want to yank back, and then
you kill something else?  C-y would yank the more recent kill.  But
the previous text is not lost.  You can get back to it using the M-y
command.  After you have done C-y to get the most recent kill, typing
M-y replaces that yanked text with the previous kill.  Typing M-y
again and again brings in earlier and earlier kills.  When you have
reached the text you are looking for, you do not have to do anything to
keep it.  Just go on with your editing, leaving the yanked text where
it is.

If you M-y enough times, you come back to the starting point (the most
recent kill).

The buffer named "*Messages*" also does not correspond to any file.
This buffer contains the messages that have appeared on the bottom
line during your Emacs session.





>> Type C-x b *Messages* <Return> to look at the buffer of messages.
   Then type C-x b TUTORIAL <Return> to come back to this tutorial.

If you make changes to the text of one file, then find another file,
this does not save the first file.  Its changes remain inside Emacs,
in that file's buffer.  The creation or editing of the second file's
buffer has no effect on the first file's buffer.  This is very useful,
but it also means that you need a convenient way to save the first
file's buffer.  It would be a nuisance to have to switch back to
it with C-x C-f in order to save it with C-x C-s.  So we have

    C-x s     Save some buffers

C-x s asks you about each buffer which contains changes that you have
not saved.  It asks you, for each such buffer, whether to save the
buffer.

>> Insert a line of text, then type C-x s.
   It should ask you whether to save the buffer named TUTORIAL.
   Answer yes to the question by typing "y".


** EXTENDING THE COMMAND SET
---------------------------

There are many, many more Emacs commands than could possibly be put
on all the control and meta characters.  Emacs gets around this with
the X (eXtend) command.  This comes in two flavors:

    C-x	Character eXtend.  Followed by one character.
    M-x	Named command eXtend.  Followed by a long name.

These are commands that are generally useful but are used less than the
commands you have already learned about.  You have already seen a few
of them: the file commands C-x C-f to Find and C-x C-s to Save, for
example.  Another example is the command to end the Emacs
session--this is the command C-x C-c.  (Do not worry about losing
changes you have made; C-x C-c offers to save each changed file before
it kills Emacs.)

If you are using a graphical display, you don't need any special
command to move from Emacs to another application.  You can do this
with the mouse or with window manager commands.  However, if you're
using a text terminal which can only show one application at a time,
you need to "suspend" Emacs to move to any other program.

C-z is the command to exit Emacs **temporarily**--so that you can go
back to the same Emacs session afterward.  When Emacs is running on a
text terminal, C-z "suspends" Emacs; that is, it returns to the shell
but does not destroy the Emacs job.  In the most common shells, you
can resume Emacs with the `fg' command or with `%emacs'.

The time to use C-x C-c is when you are about to log out.  It's also
the right thing to use to exit an Emacs invoked under mail handling
programs and other miscellaneous utilities.

There are many C-x commands.  Here is a list of the ones you have learned:

    C-x C-f		Find file
    C-x C-s		Save file
    C-x s		Save some buffers
    C-x C-b		List buffers
    C-x b		Switch buffer
    C-x C-c		Quit Emacs
    C-x 1		Delete all but one window
    C-x u		Undo

Named eXtended commands are commands which are used even less
frequently, or commands which are used only in certain modes.  An
example is the command replace-string, which globally replaces one
string with another.  When you type M-x, Emacs prompts you at the
bottom of the screen with M-x and you should type the name of the
command; in this case, "replace-string".  Just type "repl s<TAB>" and
Emacs will complete the name.  (<TAB> is the Tab key, usually found
above the CapsLock or Shift key near the left edge of the keyboard.)
Submit the command name with <Return>.

The replace-string command requires two arguments--the string to be
replaced, and the string to replace it with.  You must end each
argument with <Return>.

>> Move the cursor to the blank line two lines below this one.
   Then type M-x repl s<Return>changed<Return>altered<Return>.

   Notice how this line has changed: you've replaced
   the word c-h-a-n-g-e-d with "altered" wherever it occurred,
   after the initial position of the cursor.


** AUTO SAVE
-----------

When you have made changes in a file, but you have not saved them yet,
they could be lost if your computer crashes.  To protect you from
this, Emacs periodically writes an "auto save" file for each file that
you are editing.  The auto save file name has a # at the beginning and
the end; for example, if your file is named "hello.c", its auto save
file's name is "#hello.c#".  When you save the file in the normal way,
Emacs deletes its auto save file.

If the computer crashes, you can recover your auto-saved editing by
finding the file normally (the file you were editing, not the auto
save file) and then typing M-x recover-file <Return>.  When it asks for
confirmation, type yes<Return> to go ahead and recover the auto-save
data.


** ECHO AREA
-----------

If Emacs sees that you are typing multicharacter commands slowly, it
shows them to you at the bottom of the screen in an area called the
"echo area".  The echo area contains the bottom line of the screen.


** MODE LINE
-----------

The line immediately above the echo area is called the "mode line".
The mode line says something like this:

 -:****-  TUTORIAL       63% L749    (Fundamental)

This line gives useful information about the status of Emacs and
the text you are editing.

You already know what the filename means--it is the file you have
found.  NN% indicates your current position in the buffer text; it
means that NN percent of the buffer is above the top of the screen.
If the top of the buffer is on the screen, it will say "Top" instead
of " 0%".  If the bottom of the buffer is on the screen, it will say
"Bot".  If you are looking at a buffer so small that all of it fits on
the screen, the mode line says "All".

The L and digits indicate position in another way: they give the
current line number of point.

The stars near the front mean that you have made changes to the text.
Right after you visit or save a file, that part of the mode line shows
no stars, just dashes.

The part of the mode line inside the parentheses is to tell you what
editing modes you are in.  The default mode is Fundamental which is
what you are using now.  It is an example of a "major mode".

Emacs has many different major modes.  Some of them are meant for
editing different languages and/or kinds of text, such as Lisp mode,
Text mode, etc.  At any time one and only one major mode is active,
and its name can always be found in the mode line just where
"Fundamental" is now.

Each major mode makes a few commands behave differently.  For example,
there are commands for creating comments in a program, and since each
programming language has a different idea of what a comment should
look like, each major mode has to insert comments differently.  Each
major mode is the name of an extended command, which is how you can
switch to that mode.  For example, M-x fundamental-mode is a command to
switch to Fundamental mode.

If you are going to be editing human-language text, such as this file, you
should probably use Text Mode.

>> Type M-x text-mode <Return>.

Don't worry, none of the Emacs commands you have learned changes in
any great way.  But you can observe that M-f and M-b now treat
apostrophes as part of words.  Previously, in Fundamental mode,
M-f and M-b treated apostrophes as word-separators.

Major modes usually make subtle changes like that one: most commands
do "the same job" in each major mode, but they work a little bit
differently.

To view documentation on your current major mode, type C-h m.

>> Type C-l C-l to bring this line to the top of screen.
>> Type C-h m, to see how Text mode differs from Fundamental mode.
>> Type C-x 1 to remove the documentation from the screen.

Major modes are called major because there are also minor modes.
Minor modes are not alternatives to the major modes, just minor
modifications of them.  Each minor mode can be turned on or off by
itself, independent of all other minor modes, and independent of your
major mode.  So you can use no minor modes, or one minor mode, or any
combination of several minor modes.

One minor mode which is very useful, especially for editing
human-language text, is Auto Fill mode.  When this mode is on, Emacs
breaks the line in between words automatically whenever you insert
text and make a line that is too wide.

You can turn Auto Fill mode on by doing M-x auto-fill-mode <Return>.
When the mode is on, you can turn it off again by doing
M-x auto-fill-mode <Return>.  If the mode is off, this command turns
it on, and if the mode is on, this command turns it off.  We say that
the command "toggles the mode".

>> Type M-x auto-fill-mode <Return> now.  Then insert a line of "asdf "
   over again until you see it divide into two lines.  You must put in
   spaces between them because Auto Fill breaks lines only at spaces.

The margin is usually set at 70 characters, but you can change it
with the C-x f command.  You should give the margin setting you want
as a numeric argument.

>> Type C-x f with an argument of 20.  (C-u 2 0 C-x f).
   Then type in some text and see Emacs fill lines of 20
   characters with it.  Then set the margin back to 70 using
   C-x f again.

If you make changes in the middle of a paragraph, Auto Fill mode
does not re-fill it for you.
To re-fill the paragraph, type M-q (META-q) with the cursor inside
that paragraph.

>> Move the cursor into the previous paragraph and type M-q.


** SEARCHING
-----------

Emacs can do searches for strings (a "string" is a group of contiguous
characters) either forward through the text or backward through it.
Searching for a string is a cursor motion command; it moves the cursor
to the next place where that string appears.

The Emacs search command is "incremental".  This means that the
search happens while you type in the string to search for.

The command to initiate a search is C-s for forward search, and C-r
for reverse search.  BUT WAIT!  Don't try them now.

When you type C-s you'll notice that the string "I-search" appears as
a prompt in the echo area.  This tells you that Emacs is in what is
called an incremental search waiting for you to type the thing that
you want to search for.  <Return> terminates a search.

>> Now type C-s to start a search.  SLOWLY, one letter at a time,
   type the word 'cursor', pausing after you type each
   character to notice what happens to the cursor.
   Now you have searched for "cursor", once.
>> Type C-s again, to search for the next occurrence of "cursor".
>> Now type <DEL> four times and see how the cursor moves.
>> Type <Return> to terminate the search.

Did you see what happened?  Emacs, in an incremental search, tries to
go to the occurrence of the string that you've typed out so far.  To
go to the next occurrence of 'cursor' just type C-s again.  If no such
occurrence exists, Emacs beeps and tells you the search is currently
"failing".  C-g would also terminate the search.

If you are in the middle of an incremental search and type <DEL>, the
search "retreats" to an earlier location.  If you type <DEL> just
after you had typed C-s to advance to the next occurrence of a search
string, the <DEL> moves the cursor back to an earlier occurrence.  If
there are no earlier occurrences, the <DEL> erases the last character
in the search string.  For instance, suppose you have typed "c", to
search for the first occurrence of "c".  Now if you type "u", the
cursor will move to the first occurrence of "cu".  Now type <DEL>.
This erases the "u" from the search string, and the cursor moves back
to the first occurrence of "c".

If you are in the middle of a search and type a control or meta
character (with a few exceptions--characters that are special in a
search, such as C-s and C-r), the search is terminated.

C-s starts a search that looks for any occurrence of the search string
AFTER the current cursor position.  If you want to search for
something earlier in the text, type C-r instead.  Everything that we
have said about C-s also applies to C-r, except that the direction of
the search is reversed.


** MULTIPLE WINDOWS
------------------

One of the nice features of Emacs is that you can display more than
one window on the screen at the same time.  (Note that Emacs uses the
term "frames"--described in the next section--for what some other
applications call "windows".  The Emacs manual contains a Glossary of
Emacs terms.)

>> Move the cursor to this line and type C-l C-l.

>> Now type C-x 2 which splits the screen into two windows.
   Both windows display this tutorial.  The editing cursor stays in
   the top window.

>> Type C-M-v to scroll the bottom window.
   (If you do not have a real META key, type <ESC> C-v.)

>> Type C-x o ("o" for "other") to move the cursor to the bottom window.
>> Use C-v and M-v in the bottom window to scroll it.
   Keep reading these directions in the top window.

>> Type C-x o again to move the cursor back to the top window.
   The cursor in the top window is just where it was before.

You can keep using C-x o to switch between the windows.  The "selected
window", where most editing takes place, is the one with a prominent
cursor which blinks when you are not typing.  The other windows have
their own cursor positions; if you are running Emacs in a graphical
display, those cursors are drawn as unblinking hollow boxes.

The command C-M-v is very useful when you are editing text in one
window and using the other window just for reference.  Without leaving
the selected window, you can scroll the other window with C-M-v.

C-M-v is an example of a CONTROL-META character.  If you have a META
(or Alt) key, you can type C-M-v by holding down both CONTROL and META
while typing v.  It does not matter whether CONTROL or META "comes
first," as both of these keys act by modifying the characters you
type.

If you do not have a META key, and you use <ESC> instead, the order
does matter: you must type <ESC> followed by CONTROL-v, because
CONTROL-<ESC> v will not work.  This is because <ESC> is a character
in its own right, not a modifier key.

>> Type C-x 1 (in the top window) to get rid of the bottom window.

(If you had typed C-x 1 in the bottom window, that would get rid
of the top one.  Think of this command as "Keep just one
window--the window I am already in.")

You do not have to display the same buffer in both windows.  If you
use C-x C-f to find a file in one window, the other window does not
change.  You can find a file in each window independently.

Here is another way to use two windows to display two different things:

>> Type C-x 4 C-f followed by the name of one of your files.
   End with <Return>.  See the specified file appear in the bottom
   window.  The cursor goes there, too.

>> Type C-x o to go back to the top window, and C-x 1 to delete
   the bottom window.


** MULTIPLE FRAMES
------------------

Emacs can also create multiple "frames".  A frame is what we call one
collection of windows, together with its menus, scroll bars, echo
area, etc.  On graphical displays, what Emacs calls a "frame" is what
most other applications call a "window".  Multiple graphical frames
can be shown on the screen at the same time.  On a text terminal, only
one frame can be shown at a time.

>> Type M-x make-frame <Return>.
   See a new frame appear on your screen.

You can do everything you did in the original frame in the new frame.
There is nothing special about the first frame.

>> Type M-x delete-frame <Return>.
   This removes the selected frame.

You can also remove a frame by using the normal method provided by the
graphical system (often clicking a button with an "X" at a top corner
of the frame).  If you remove the Emacs job's last frame this way,
that exits Emacs.


** RECURSIVE EDITING LEVELS
--------------------------

Sometimes you will get into what is called a "recursive editing
level".  This is indicated by square brackets in the mode line,
surrounding the parentheses around the major mode name.  For
example, you might see [(Fundamental)] instead of (Fundamental).

To get out of the recursive editing level, type <ESC> <ESC> <ESC>.
That is an all-purpose "get out" command.  You can also use it for
eliminating extra windows, and getting out of the minibuffer.

>> Type M-x to get into a minibuffer; then type <ESC> <ESC> <ESC> to
   get out.

You cannot use C-g to get out of a recursive editing level.  This is
because C-g is used for canceling commands and arguments WITHIN the
recursive editing level.


** GETTING MORE HELP
-------------------

In this tutorial we have tried to supply just enough information to
get you started using Emacs.  There is so much available in Emacs that
it would be impossible to explain it all here.  However, you may want
to learn more about Emacs since it has many other useful features.
Emacs provides commands for reading documentation about Emacs
commands.  These "help" commands all start with the character
CONTROL-h, which is called "the Help character".

To use the Help features, type the C-h character, and then a
character saying what kind of help you want.  If you are REALLY lost,
type C-h ? and Emacs will tell you what kinds of help it can give.
If you have typed C-h and decide you do not want any help, just
type C-g to cancel it.

(If C-h does not display a message about help at the bottom of the
screen, try typing the F1 key or M-x help <Return> instead.)

The most basic HELP feature is C-h c.  Type C-h, the character c, and
a command character or sequence; then Emacs displays a very brief
description of the command.

>> Type C-h c C-p.

The message should be something like this:

    C-p runs the command previous-line

This tells you the "name of the function".  Since function names
are chosen to indicate what the command does, they can serve as
very brief documentation--sufficient to remind you of commands you
have already learned.

Multi-character commands such as C-x C-s and (if you have no META or
EDIT or ALT key) <ESC>v are also allowed after C-h c.

To get more information about a command, use C-h k instead of C-h c.

>> Type C-h k C-p.

This displays the documentation of the function, as well as its name,
in an Emacs window.  When you are finished reading the output, type
C-x 1 to get rid of that window.  You do not have to do this right
away.  You can do some editing while referring to the help text, and
then type C-x 1.

Here are some other useful C-h options:

   C-h f	Describe a function.  You type in the name of the
        function.

>> Try typing C-h f previous-line <Return>.
   This displays all the information Emacs has about the
   function which implements the C-p command.

A similar command C-h v displays the documentation of variables,
including those whose values you can set to customize Emacs behavior.
You need to type in the name of the variable when Emacs prompts for it.

   C-h a	Command Apropos.  Type in a keyword and Emacs will list
        all the commands whose names contain that keyword.
        These commands can all be invoked with META-x.
        For some commands, Command Apropos will also list a one
        or two character sequence which runs the same command.

>> Type C-h a file <Return>.

This displays in another window a list of all M-x commands with "file"
in their names.  You will see character-commands like C-x C-f listed
beside the corresponding command names such as find-file.

>> Type C-M-v to scroll the help window.  Do this a few times.

>> Type C-x 1 to delete the help window.

   C-h i	Read included Manuals (a.k.a. Info).  This command puts
        you into a special buffer called `**info**' where you
        can read manuals for the packages installed on your system.
        Type m emacs <Return> to read the Emacs manual.
        If you have never before used Info, type ? and Emacs
        will take you on a guided tour of Info mode facilities.
        Once you are through with this tutorial, you should
        consult the Emacs Info manual as your primary documentation.


** MORE FEATURES
---------------

You can learn more about Emacs by reading its manual, either as a
printed book, or inside Emacs (use the Help menu or type C-h r).
Two features that you may like especially are completion, which saves
typing, and dired, which simplifies file handling.

Completion is a way to avoid unnecessary typing.  For instance, if you
want to switch to the **Messages** buffer, you can type C-x b **M<Tab>
and Emacs will fill in the rest of the buffer name as far as it can
determine from what you have already typed.  Completion also works for
command names and file names.  Completion is described in the Emacs
manual in the node called "Completion".

Dired enables you to list files in a directory (and optionally its
subdirectories), move around that list, visit, rename, delete and
otherwise operate on the files.  Dired is described in the Emacs
manual in the node called "Dired".

The manual also describes many other Emacs features.


** CONCLUSION
------------

To exit Emacs use C-x C-c.

This tutorial is meant to be understandable to all new users, so if
you found something unclear, don't sit and blame yourself - complain!


** COPYING
---------

This tutorial descends from a long line of Emacs tutorials
starting with the one written by Stuart Cracraft for the original Emacs.

This version of the tutorial is a part of GNU Emacs.  It is copyrighted
and comes with permission to distribute copies on certain conditions:

  Copyright (C) 1985, 1996, 1998, 2001-2012 Free Software Foundation, Inc.

  This file is part of GNU Emacs.

  GNU Emacs is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  GNU Emacs is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

Please read the file COPYING and then do give copies of GNU Emacs to
your friends.  Help stamp out software obstructionism ("ownership") by
using, writing, and sharing free software!


* Modules

_References:_

 - [[http://diobla.info/blog-archive/modules-tut.html][Introduction to Emacs modules]]
 - [[http://nullprogram.com/blog/2016/11/05/][Emacs, Dynamic Modules, and Joysticks]]
