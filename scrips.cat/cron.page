---
format: Org
categories: scripts
toc: yes
title: Cron
content: Cront content
...

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[#desktop][Desktop]]
- [[#server][Server]]

* Desktop

#+begin_src sh :tangle cron.sh :padline no
  # <minute> <hour> <day_of_month> <month> <day_of_week> <command>
  DISPLAY=:0.0
  #
  #   PACBUILDER
  #
  0 12 * * * sudo abs && pacbuilder --noresume --builddeps --keepdeps --noconfirm --install {tmux,zsh,awesome,arch-wiki-docs} 2>&1
  #
  #   MODPROBED-DB
  #
  # * * * * *   /usr/bin/modprobed-db store &> /dev/null
  #
  #   HOSTSBLOCK
  #
  # 0 0 * * * sudo /usr/sbin/hostsblock 2>&1
  #
  #   HOSTS-UPDATE
  #
  # 0 12 * * * sudo hosts-update 2>&1
  #
  #   Octave
  #
  #   Recompile Octave from sources
  #
  # 0 5 * * 1 yaourt -S --noconfirm octave-hg 2>&1
  #
  #   update emacs libraries
  #
  # 0 0 2 * * /home/csantos/.emacs.d/libraries/update.sh 2>&1
  #
  #   Update helm dash docsets
  #
  # 0 3 * *  3 /usr/bin/emacsclient --eval "(helm-dash-update-all-docsets)" 2>&1
  #
  #   DB UPDATE
  #
  # 0 0 * * * sudo updatedb 2>&1
  #
  #   SYNCTHING - restart several times a day
  #
  0 10,13,18 * * * systemctl --user restart syncthing-inotify 2>&1
  #
  #    PKGFILE
  #
  0 12 * * * sudo pkgfile --update 2>&1
  #
  #    RECOLL
  #
  # 0 12  * * * RCLCRON_RCLINDEX= RECOLL_CONFDIR="/home/csantos/.recoll/" recollindex 2>&1
  #
  #    PSD - resync profiles every 10 minutes
  #
  # *0 * * * * sudo /usr/bin/profile-sync-daemon resync
  #
  #    ASD - resync anything every 10 minutes
  #
  # *0 * * * * sudo /usr/bin/anything-sync-daemon resync
  #
  #    PYLOOKUP
  #
  # 0 0 * * 3 /home/csantos/Dropbox/scripts/pylookup_updatedb_3
  # 0 0 * * 3 /home/csantos/Dropbox/scripts/pylookup_updatedb_2
  #
  #    update pentadactyl plugins
  #
  # 30 1 * * * /home/csantos/.pentadactyl/plugins/update.sh
  #
  #    Reconstruct db of mail alias from notmutch db
  #
  # 0 3 * * 2 /home/csantos/.mutt/notmuch-new.sh all
  # 1 3 * * 2 /home/csantos/.mutt/nottoomuch-addresses.sh --update --rebuild
  #
  # 00 08 * * * vlc /home/csantos/Documents/Perso/france_info.m3u 2>&1
  # 00 9,12,15,18,21 * * 5,6 newsbeuter -c ~/mycache/newsbeuter/nb_cache.db -x reload 2>&1
  # 00 22 * * 0-4 newsbeuter -c ~/mycache/newsbeuter/nb_cache.db -x reload 2>&1
  # *5 * * * * rsync ~/mycache/newsbeuter/nb_cache.db ~/nb_cache.db 2>&1
  # 00 00 * * * sudo updatedb 2>&1
  # 00 00 * * * goobook -c ~/.mutt/.goobookrc_perso reload
  # 00 00 * * * goobook -c ~/.mutt/.goobookrc_curro reload
  # 0 0  * * * RCLCRON_RCLINDEX= RECOLL_CONFDIR="/home/csantos/.recoll/" recollindex
  # 0 0 * * 1 /home/csantos/Dropbox/scripts/pylookup_updatedb
  #
  # full backup twice a month, at 00:00 am
  # 0 0 1,15 * * /home/csantos/Dropbox/scripts/backup_btsync_full.sh
  #
  # inc backup every day at 00:00 am
  # 0 0 2-14,16-31 * * /home/csantos/Dropbox/scripts/backup_btsync_inc.sh
  #
  # stop syncing during the day to save SD acceses
  # 0 9 * * * sudo systemctl stop btsync@csantos
  #
  # start syncing in the afternoon
  # 30 19-23 * * * sudo systemctl start btsync@csantos
  #
  #   To have rss2email check for new feeds every 10 minutes: 'r2e' is the alias !
  #
  # 0 * * * * /usr/bin/r2e --config ~/Dropbox/config/rss2email/rss2email.cfg --data ~/Dropbox/config/rss2email/rss2email.json run
  #
  # update online aliases
  #
  # 10 0 * * * wget -q -O - "$@" https://alias.sh/user/2204/alias > ~/.zsh/aliases_online.zsh
  #
#+end_src

* Server

#+begin_src sh :tangle cron.sh :padline no
  # <minute> <hour> <day_of_month> <month> <day_of_week> <command>
  DISPLAY=:0.0
  #
  #    PKGFILE
  #
  0 0 * * * sudo pkgfile --update 2>&1
  #
  #
  #   MINIFLUX
  #
  0 * * * *  cd /srv/http/miniflux && php cronjob.php 2>&1
  0 1 * * *  cd /srv/http/miniflux && sudo git pull 2>&1
  5 1 * * *  sudo systemctl restart httpd 2>&1
  #
  #   WIKIDATA
  #
  *0 * * * *  cd /home/csantos/Projects/perso/wikidata && git pull 2>&1
  *5 * * * *  /usr/sbin/systemctl --user restart gitit-online 2>&1
  #
  #   SYNCTHING - restart several times a day
  #
  0 10,13,18 * * * systemctl --user restart syncthing-inotify 2>&1
#+end_src
