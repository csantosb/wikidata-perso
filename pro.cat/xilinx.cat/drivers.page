---
format: Org
categories: pro, xilinx
toc: yes
title: Xilin USB-JTAG Drivers
content: Make Xilinx USB-JTAG work under arch
...

To make Xilinx USB-JTAG work under arch follow this guide.

* Install

It is necessary to install =libusb-compat=, =fxload= and =digilent.adept.runtime=.

* Configuration

To setup things, run

#+begin_src sh
  mkdir -p /opt/Xilinx/14.7/ISE_DS/ISE/lib/lin64/plugins/Digilent/libCseDigilent
  cd /opt/Xilinx/14.7/ISE_DS/ISE/bin/lin64/digilent/libCseDigilent_2.4.4-x86_64/lin64/14.1/libCseDigilent
  cp libCseDigilent.{so,xml} /opt/Xilinx/14.7/ISE_DS/ISE/lib/lin64/plugins/Digilent/libCseDigilent
  chmod -x /opt/Xilinx/14.7/ISE_DS/ISE/lib/lin64/plugins/Digilent/libCseDigilent/libCseDigilent.xml
#+end_src

and install the =usb-driver= code as follows.

#+begin_src sh
  cd /opt/Xilinx
  sudo git clone git://git.zerfleddert.de/usb-driver
  cd usb-driver/
  sudo make

  ./setup_pcusb /opt/Xilinx/14.7/ISE_DS/ISE/
#+end_src

The =udev= rules file will be created. Update with

#+begin_src sh
  sudo udevadm control --reload-rules
#+end_src

and insert the =parport_pc= module.

#+begin_src sh
  sudo modprobe parport_pc
#+end_src

Finally, add every user that should have access to the Digilent USB-JTAG adapter to the "uucp" group.

And don’t forget that, o grant access to the usb driver for normal users you may
have to add the USB Vendor/Product IDs of your JTAG adapter which can be found
with

#+begin_src emacs-lisp
  lsusb | grep Xilinx
#+end_src

to the udev rules in /etc/udev/rules.d/20-digilent.rules:

#+begin_src emacs-lisp
  SUBSYSTEM=="usb", ATTRS{idVendor}=="xxxx", ATTRS{idProduct}=="xxxx", GROUP="users", MODE="666"
#+end_src

* References

- https://wiki.archlinux.org/index.php/Xilinx_ISE_WebPACK

In particular

- https://wiki.archlinux.org/index.php/Xilinx_ISE_WebPACK#Digilent_USB-JTAG_Drivers
- https://wiki.archlinux.org/index.php/Xilinx_ISE_WebPACK#Xilinx_Platform_Cable_USB-JTAG_Drivers
