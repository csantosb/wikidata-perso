---
format: Org
categories: software zsh shell fzf
toc: yes
title: fzf oh-my-zsh plugin
content: fzf plugin for oh-my-zsh
...

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[#prose][Prose]]
  - [[#theme][Theme]]

* Prose
:PROPERTIES:
:header-args:    :tangle /home/csantos/DotFiles/oh-my-zsh/custom/themes/prose.zsh-theme :mkdirp yes
:END:

#+begin_src sh :padline no
  #######################################################################
  #  DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING PAGE !!! #
  #######################################################################
#+end_src

** Theme

#+begin_src sh
  if [ "x$OH_MY_ZSH_HG" = "x" ]; then
                  OH_MY_ZSH_HG="hg"
  fi

  function box_name { uname -n | cut -d. -f1 }

  PROMPT='
  $fg_bold[green]%}[%{$fg[yellow]%}$(box_name):%{$reset_color%} %{$fg_bold[green]%}${PWD/#$HOME/~}]%{$reset_color%}$(git_prompt_info)
  %(?,,%{${fg_bold[white]}%}[%?]%{$reset_color%} )$ '

  ZSH_THEME_GIT_PROMPT_PREFIX=" @ %{$fg[magenta]%}"
  ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
  ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[green]%}!"
  ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[green]%}?"
  ZSH_THEME_GIT_PROMPT_CLEAN=""

  local return_status="%{$fg[red]%}%(?..✘)%{$reset_color%}"
  RPROMPT='${return_status}%{$reset_color%}'
#+end_src
