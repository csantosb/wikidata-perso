---
format: Org
categories: software cli
toc: yes
title: Rofi
content: Rofi related
...

* Table of Contents        :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[#themes][Themes]]
- [[#surfraw][Surfraw]]
- [[#rofi-surfraw][Rofi surfraw]]
  - [[#config][config]]
  - [[#searchengines][searchengines]]
- [[#password-manager][Password manager]]
- [[#rofi-pass][Rofi pass]]

* rofi

** themes

Under

#+begin_src sh
 /usr/share/rofi/themes
#+end_src

Select with

#+begin_src sh
 rofi-theme-selector
#+end_src

** configuration
:PROPERTIES:
:header-args: :tangle /home/csantos/DotFiles/rofi/config :mkdirp yes
:END:

To get a list of available options formatted as Xresources entries, run:

#+begin_src sh :tangle no
  rofi -dump-Xresources
#+end_src

or in a more readable format:

#+begin_src sh :tangle no
  rofi -help
#+end_src

#+begin_src sh :padline no
  #######################################################################
  #  DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING PAGE !!! #
  #######################################################################
#+end_src

#+begin_src sh
  ! rofi -- dmenu clone

  ! rofi.opacity:  100
  ! rofi.color-enabled: true

  ! State:           bg,     fg,     bgalt   hlbg,   hlfg
  ! zenburn:         bg,     fg,     bg-05,  bg+1,   fg+1
  ! rofi.color-normal: #3F3F3F,#DCDCCC,#383838,#4F4F4F,#FFFFEF
  !                  blue-4, fg,     blue-5, blue-3, fg+1
  ! rofi.color-active: #4C7073,#DCDCCC,#366060,#5C888B,#FFFFEF
  !                  red-4,  fg,     red-5,  red-3,  fg+1
  ! rofi.color-urgent: #8C5353,#DCDCCC,#7C4343,#9C6363,#FFFFEF

  ! "Enabled modi" Set from: Default
  rofi.modi:                           window,run,ssh,drun,combi,windowcd,keys
  ! "Window width" Set from: File
  rofi.width:                          50
  ! "Number of lines" Set from: File
  rofi.lines:                          10
  ! "Number of columns" Set from: Default
  ! rofi.columns:                        1
  ! "Font to use" Set from: File
  rofi.font:                           Inconsolata 12
  ! "Border width" Set from: File
  rofi.bw:                             0
  ! "Location on screen" Set from: File
  rofi.location:                       2
  ! "Padding" Set from: File
  rofi.padding:                        5
  ! "Y-offset relative to location" Set from: Default
  ! rofi.yoffset:                        0
  ! "X-offset relative to location" Set from: Default
  ! rofi.xoffset:                        0
  ! "Always show number of lines" Set from: File
  rofi.fixed-num-lines:                false
  ! "Whether to load and show icons" Set from: Default
  ! rofi.show-icons:                     false
  ! "Terminal to use" Set from: Default
  ! rofi.terminal:                       rofi-sensible-terminal
  ! "Ssh client to use" Set from: Default
  ! rofi.ssh-client:                     ssh
  ! "Ssh command to execute" Set from: Default
  ! rofi.ssh-command:                    {terminal} -e {ssh-client} {host}
  ! "Run command to execute" Set from: Default
  ! rofi.run-command:                    {cmd}
  ! "Command to get extra run targets" Set from: Default
  ! rofi.run-list-command:
  ! "Run command to execute that runs in shell" Set from: Default
  ! rofi.run-shell-command:              {terminal} -e {cmd}
  ! "Command executed on accep-entry-custom for window modus" Set from: Default
  ! rofi.window-command:                 xkill -id {window}
  ! "Window fields to match in window mode" Set from: Default
  ! rofi.window-match-fields:            all
  ! "Theme to use to look for icons" Set from: Default
  ! rofi.drun-icon-theme:
  ! "Desktop entry fields to match in drun" Set from: Default
  ! rofi.drun-match-fields:              all
  ! "Disable history in run/ssh" Set from: Default
  ! rofi.disable-history:                false
  ! "Use sorting" Set from: Default
  ! rofi.sort:                           false
  ! "Use levenshtein sorting also for fuzzy matching" Set from: Default
  ! rofi.levenshtein-sort:               false
  ! "Set case-sensitivity" Set from: Default
  ! rofi.case-sensitive:                 false
  ! "Cycle through the results list" Set from: Default
  ! rofi.cycle:                          true
  ! "Enable sidebar-mode" Set from: Default
  ! rofi.sidebar-mode:                   false
  ! "Row height (in chars)" Set from: Default
  ! rofi.eh:                             1
  ! "Enable auto select mode" Set from: Default
  ! rofi.auto-select:                    false
  ! "Parse hosts file for ssh mode" Set from: Default
  ! rofi.parse-hosts:                    false
  ! "Parse known_hosts file for ssh mode" Set from: Default
  ! rofi.parse-known-hosts:              true
  ! "Set the modi to combine in combi mode" Set from: Default
  ! rofi.combi-modi:                     window,run
  ! "Set the matching algorithm. (normal, regex, glob, fuzzy)" Set from: Default
  ! rofi.matching:                       normal
  ! "Tokenize input string" Set from: Default
  ! rofi.tokenize:                       true
  ! "Monitor id to show on" Set from: Default
  ! rofi.m:                              -5
  ! "Margin between rows *DEPRECATED*" Set from: Default
  ! rofi.line-margin:                    2
  ! "Padding within rows *DEPRECATED*" Set from: Default
  ! rofi.line-padding:                   1
  ! "Pre-set filter" Set from: Default
  ! rofi.filter:
  ! "Separator style (none, dash, solid) *DEPRECATED*" Set from: Default
  ! rofi.separator-style:                dash
  ! "Hide scroll-bar *DEPRECATED*" Set from: File
  rofi.hide-scrollbar:                 true
  ! "Fullscreen" Set from: Default
  ! rofi.fullscreen:                     false
  ! "Fake transparency *DEPRECATED*" Set from: Default
  ! rofi.fake-transparency:              false
  ! "DPI" Set from: Default
  ! rofi.dpi:                            -1
  ! "Threads to use for string matching" Set from: Default
  ! rofi.threads:                        0
  ! "Scrollbar width *DEPRECATED*" Set from: Default
  ! rofi.scrollbar-width:                8
  ! "Scrolling method. (0: Page, 1: Centered)" Set from: Default
  ! rofi.scroll-method:                  0
  ! "Background to use for fake transparency. (background or screenshot)" Set from: Default
  ! rofi.fake-background:                screenshot
  ! "Window Format. w (desktop name), t (title), n (name), r (role), c (class) *DEPRECATED*" Set from: Default
  ! rofi.window-format:                  {w}    {i}{c}   {t}
  ! "Click outside the window to exit" Set from: Default
  ! rofi.click-to-exit:                  true
  ! "Indicate how it match by underlining it." Set from: Default
  ! rofi.show-match:                     true
  ! "New style theme file" Set from: Default
  ! rofi.theme: dmenu
  ! "Color scheme for normal row" Set from: File
  ! rofi.color-normal:                   #3F3F3F,#DCDCCC,#383838,#4F4F4F,#FFFFEF
  ! "Color scheme for urgent row" Set from: File
  ! rofi.color-urgent:                   #8C5353,#DCDCCC,#7C4343,#9C6363,#FFFFEF
  ! "Color scheme for active row" Set from: File
  ! rofi.color-active:                   #4C7073,#DCDCCC,#366060,#5C888B,#FFFFEF
  ! "Color scheme window" Set from: File
  ! rofi.color-window:                   #3F3F3F,#000000
  ! rofi.theme:
  ! "Color scheme for normal row" Set from: Default
  ! rofi.color-normal:
  ! "Color scheme for urgent row" Set from: Default
  ! rofi.color-urgent:
  ! "Color scheme for active row" Set from: Default
  ! rofi.color-active:
  ! "Color scheme window" Set from: Default
  ! rofi.color-window:
  ! "Max history size (WARNING: can cause slowdowns when set to high)." Set from: Default
  ! rofi.max-history-size:               25
  ! "Hide the prefix mode prefix on the combi view." Set from: Default
  ! rofi.combi-hide-mode-prefix:         false
  ! "Pidfile location" Set from: Default
  ! rofi.pid:                            /run/user/1510/rofi.pid
  ! "The display name of this browser" Set from: Default
  ! rofi.display-window:
  ! "The display name of this browser" Set from: Default
  ! rofi.display-windowcd:
  ! "The display name of this browser" Set from: Default
  ! rofi.display-run:
  ! "The display name of this browser" Set from: Default
  ! rofi.display-ssh:
  ! "The display name of this browser" Set from: Default
  ! rofi.display-drun:
  ! "The display name of this browser" Set from: Default
  ! rofi.display-combi:
  ! "The display name of this browser" Set from: Default
  ! rofi.display-keys:
  ! "Paste primary selection" Set from: Default
  ! rofi.kb-primary-paste:               Control+V,Shift+Insert
  ! "Paste clipboard" Set from: Default
  ! rofi.kb-secondary-paste:             Control+v,Insert
  ! "Clear input line" Set from: Default
  ! rofi.kb-clear-line:                  Control+w
  ! "Beginning of line" Set from: Default
  ! rofi.kb-move-front:                  Control+a
  ! "End of line" Set from: Default
  ! rofi.kb-move-end:                    Control+e
  ! "Move back one word" Set from: Default
  ! rofi.kb-move-word-back:              Alt+b
  ! "Move forward one word" Set from: Default
  ! rofi.kb-move-word-forward:           Alt+f
  ! "Move back one char" Set from: Default
  ! rofi.kb-move-char-back:              Left,Control+b
  ! "Move forward one char" Set from: Default
  ! rofi.kb-move-char-forward:           Right,Control+f
  ! "Delete previous word" Set from: Default
  ! rofi.kb-remove-word-back:            Control+Alt+h,Control+BackSpace
  ! "Delete next word" Set from: Default
  ! rofi.kb-remove-word-forward:         Control+Alt+d
  ! "Delete next char" Set from: Default
  ! rofi.kb-remove-char-forward:         Delete,Control+d
  ! "Delete previous char" Set from: Default
  ! rofi.kb-remove-char-back:            BackSpace,Control+h
  ! "Delete till the end of line" Set from: Default
  ! rofi.kb-remove-to-eol:               Control+k
  ! "Delete till the start of line" Set from: Default
  ! rofi.kb-remove-to-sol:               Control+u
  ! "Accept entry" Set from: Default
  ! rofi.kb-accept-entry:                Control+j,Control+m,Return,KP_Enter
  ! "Use entered text as command (in ssh/run modi)" Set from: Default
  ! rofi.kb-accept-custom:               Control+Return
  ! "Use alternate accept command." Set from: Default
  ! rofi.kb-accept-alt:                  Shift+Return
  ! "Delete entry from history" Set from: Default
  ! rofi.kb-delete-entry:                Shift+Delete
  ! "Switch to the next mode." Set from: Default
  ! rofi.kb-mode-next:                   Shift+Right,Control+Tab
  ! "Switch to the previous mode." Set from: Default
  ! rofi.kb-mode-previous:               Shift+Left,Control+ISO_Left_Tab
  ! "Go to the previous column" Set from: Default
  ! rofi.kb-row-left:                    Control+Page_Up
  ! "Go to the next column" Set from: Default
  ! rofi.kb-row-right:                   Control+Page_Down
  ! "Select previous entry" Set from: Default
  ! rofi.kb-row-up:                      Up,Control+p,ISO_Left_Tab
  ! "Select next entry" Set from: Default
  ! rofi.kb-row-down:                    Down,Control+n
  ! "Go to next row, if one left, accept it, if no left next mode." Set from: Default
  ! rofi.kb-row-tab:                     Tab
  ! "Go to the previous page" Set from: Default
  ! rofi.kb-page-prev:                   Page_Up
  ! "Go to the next page" Set from: Default
  ! rofi.kb-page-next:                   Page_Down
  ! "Go to the first entry" Set from: Default
  ! rofi.kb-row-first:                   Home,KP_Home
  ! "Go to the last entry" Set from: Default
  ! rofi.kb-row-last:                    End,KP_End
  ! "Set selected item as input text" Set from: Default
  ! rofi.kb-row-select:                  Control+space
  ! "Take a screenshot of the rofi window" Set from: Default
  ! rofi.kb-screenshot:                  Alt+S
  ! "Toggle case sensitivity" Set from: Default
  ! rofi.kb-toggle-case-sensitivity:     grave,dead_grave
  ! "Toggle sort" Set from: Default
  ! rofi.kb-toggle-sort:                 Alt+grave
  ! "Quit rofi" Set from: Default
  ! rofi.kb-cancel:                      Escape,Control+g,Control+bracketleft
  ! "Custom keybinding 1" Set from: Default
  ! rofi.kb-custom-1:                    Alt+1
  ! "Custom keybinding 2" Set from: Default
  ! rofi.kb-custom-2:                    Alt+2
  ! "Custom keybinding 3" Set from: Default
  ! rofi.kb-custom-3:                    Alt+3
  ! "Custom keybinding 4" Set from: Default
  ! rofi.kb-custom-4:                    Alt+4
  ! "Custom Keybinding 5" Set from: Default
  ! rofi.kb-custom-5:                    Alt+5
  ! "Custom keybinding 6" Set from: Default
  ! rofi.kb-custom-6:                    Alt+6
  ! "Custom Keybinding 7" Set from: Default
  ! rofi.kb-custom-7:                    Alt+7
  ! "Custom keybinding 8" Set from: Default
  ! rofi.kb-custom-8:                    Alt+8
  ! "Custom keybinding 9" Set from: Default
  ! rofi.kb-custom-9:                    Alt+9
  ! "Custom keybinding 10" Set from: Default
  ! rofi.kb-custom-10:                   Alt+0
  ! "Custom keybinding 11" Set from: Default
  ! rofi.kb-custom-11:                   Alt+exclam
  ! "Custom keybinding 12" Set from: Default
  ! rofi.kb-custom-12:                   Alt+at
  ! "Csutom keybinding 13" Set from: Default
  ! rofi.kb-custom-13:                   Alt+numbersign
  ! "Custom keybinding 14" Set from: Default
  ! rofi.kb-custom-14:                   Alt+dollar
  ! "Custom keybinding 15" Set from: Default
  ! rofi.kb-custom-15:                   Alt+percent
  ! "Custom keybinding 16" Set from: Default
  ! rofi.kb-custom-16:                   Alt+dead_circumflex
  ! "Custom keybinding 17" Set from: Default
  ! rofi.kb-custom-17:                   Alt+ampersand
  ! "Custom keybinding 18" Set from: Default
  ! rofi.kb-custom-18:                   Alt+asterisk
  ! "Custom Keybinding 19" Set from: Default
  ! rofi.kb-custom-19:                   Alt+parenleft
  ! "Select row 1" Set from: Default
  ! rofi.kb-select-1:                    Super+1
  ! "Select row 2" Set from: Default
  ! rofi.kb-select-2:                    Super+2
  ! "Select row 3" Set from: Default
  ! rofi.kb-select-3:                    Super+3
  ! "Select row 4" Set from: Default
  ! rofi.kb-select-4:                    Super+4
  ! "Select row 5" Set from: Default
  ! rofi.kb-select-5:                    Super+5
  ! "Select row 6" Set from: Default
  ! rofi.kb-select-6:                    Super+6
  ! "Select row 7" Set from: Default
  ! rofi.kb-select-7:                    Super+7
  ! "Select row 8" Set from: Default
  ! rofi.kb-select-8:                    Super+8
  ! "Select row 9" Set from: Default
  ! rofi.kb-select-9:                    Super+9
  ! "Select row 10" Set from: Default
  ! rofi.kb-select-10:                   Super+0
  ! "Go to the previous column" Set from: Default
  ! rofi.ml-row-left:                    ScrollLeft
  ! "Go to the next column" Set from: Default
  ! rofi.ml-row-right:                   ScrollRight
  ! "Select previous entry" Set from: Default
  ! rofi.ml-row-up:                      ScrollUp
  ! "Select next entry" Set from: Default
  ! rofi.ml-row-down:                    ScrollDown
  ! "Select hovered row" Set from: Default
  ! rofi.me-select-entry:                MousePrimary
  ! "Accept hovered row" Set from: Default
  ! rofi.me-accept-entry:                MouseDPrimary
  ! "Accept hovered row with custom action" Set from: Default
  ! rofi.me-accept-custom:               Control+MouseDPrimary
#+end_src

** references

 - https://wiki.archlinux.org/index.php/Rofi
 - https://github.com/DaveDavenport/rofi/

* rofi-surfraw

#+begin_src sh
  /usr/sbin/rofi-surfraw
#+end_src

** configuration
:PROPERTIES:
:header-args: :tangle /home/csantos/DotFiles/rofi-surfraw/config :mkdirp yes
:END:

Reference configuration files

#+begin_src sh :tangle no
  /etc/rofi-pass.conf
  $HOME/.config/rofi-pass/config
#+end_src

#+begin_src sh :padline no
  #######################################################################
  #  DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING PAGE !!! #
  #######################################################################
#+end_src

#+begin_src sh
  default=google
  help_color="#0C73C2"
#+end_src

** searchengines
:PROPERTIES:
:header-args: :tangle /home/csantos/DotFiles/rofi-surfraw/searchengines :mkdirp yes
:END:

#+begin_src sh :padline no
  #######################################################################
  #  DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING PAGE !!! #
  #######################################################################
#+end_src

#+begin_src sh
  !gi - surfraw - duckduckgo !gi
  !aur - surfraw - duckduckgo !aur
  !bi - surfraw - duckduckgo !bi
  !rb - custom - https://www.rebuy.de/kaufen/suchen?q=
  !wp - surfraw - wikipedia
  !sp - custom - http://www.sputnikmusic.com/search_results.php?genreid=0&search_in=Bands&search_text=
#+end_src

** references

 - https://github.com/carnager/rofi-scripts/tree/master/rofi-surfraw

* rofi-pass

#+begin_src sh
  /usr/sbin/rofi-pass
#+end_src

** configuration
:PROPERTIES:
:header-args:    :tangle /home/csantos/DotFiles/rofi-pass/config :mkdirp yes
:END:

Reference configuration files

#+begin_src sh
  /etc/rofi-pass.conf
  $HOME/.config/rofi-pass/config
#+end_src

#+begin_src sh :padline no
  #######################################################################
  #  DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING PAGE !!! #
  #######################################################################
#+end_src

#+begin_src sh
  # permanently set alternative root dir
  # root=/path/to/root

  # /etc/rofi-pass.conf
  # /usr/sbin/rofi-pass

  # rofi command. Make sure to have "$@" as last argument
  _rofi () {
      # rofi -i -no-auto-select "$@"
      rofi -z -i -width 600 -no-levenshtein-sort -opacity 80 "$@"
  }

  # xdotool needs the keyboard layout to be set using setxkbmap
  # You can do this in your autostart scripts (e.g. xinitrc)

  # If for some reason, you cannot do this, you can set the command here.
  # and set fix_layout to true
  fix_layout=false

  layout_cmd () {
      setxkbmap -layout fr -option ctrl:nocaps
      # setxkbmap us
  }

  # fields to be used
  URL_field='url'
  USERNAME_field='user'
  AUTOTYPE_field='autotype'

  # delay to be used for :delay keyword
  delay=2

  ## Programs to be used
  # Editor
  EDITOR='/usr/bin/zile'

  # Browser
  # BROWSER='firefox --private-window'
  BROWSER="qutebrowser"

  ## Misc settings

  default_do='autopass' # menu, autotype, copyPass, typeUser, typePass, copyUser, copyUrl, viewEntry, typeMenu, actionMenu, copyMenu, openUrl
  auto_enter='false'
  notify='false'
  default_autotype='user :tab pass'

  # color of the help messages
  # leave empty for autodetection
  help_color="#4872FF"

  # Clipboard settings
  # Possible options: primary, clipboard, both
  clip=primary

  # Options for generating new password entries
  # default_user is also used for password files that have no user field.
  #default_user=john_doe
  #default_user2=mary_ann
  #password_length=12

  # Custom Keybindings
  type_user="Alt+u"
  type_pass="Alt+p"
  autotype="Alt+a"
  open_url="Alt+o"

  copy_name="Alt+2"
  copy_url="Alt+l"
  copy_pass="Alt+5"
  copy_entry="Alt+2"

  show="Alt+4"
  type_entry="Alt+1"
  copy_menu="Alt+c"
  action_menu="Alt+3"
  type_menu="Alt+t"
  help="Alt+h"
  switch="Alt+x"
  insert_pass="Alt+n"
#+end_src

** References

https://github.com/carnager/rofi-pass
[[file:/sudo:root@localhost:/etc/rofi-pass.conf][file:/sudo:root@localhost:/etc/rofi-pass.conf]]
