-- run_once("sleep 250; start-pulseaudio-x11")
-- run_once("sleep 100; synapse")
-- run_once("launchy")
-- run_once("sleep 180; owncloud")

run_once("compton --config ~/DotFiles/compton/compton.conf -b")
run_once("urxvtd -q -f -o")
run_once("setxkbmap -layout fr -option ctrl:nocaps")
-- run_once("setxkbmap -layout fr -option ctrl:swapcaps")
-- run_once("setxkbmap fr -option caps:escape")
run_once("xautolock -time 10 -locker slock")
run_once("sleep 5; pasystray")
run_once("sleep 7; cbatticon")
-- run_once("sleep 9; parrot_zik_tray")
run_once("sleep 11; udiskie --tray")
-- run_once("sleep 13; radiotray")
run_once("sleep 13; knemo")
run_once("sleep 15; unclutter -idle 2")
run_once("sleep 17; kalu")
run_once("sleep 25; pidgin")
-- run_once("sleep 35; /usr/bin/Whatsapp")
-- run_once("sleep 40; /usr/bin/sky")
-- run_once("sleep 50; /usr/bin/syncthing-gtk --minimized")
run_once("sleep 55; systemctl --user start redshift")
run_once("sleep 60; skypeforlinux")
run_once("sleep 70; franz")
run_once("sleep 170; nextcloud")
run_once("sleep 180; cernbox")

-- awful.util.spawn_with_shell("/home/csantos/Scripts/awesome-maildir-alert.sh reset")

-- awful.util.spawn_with_shell("sudo sh -c 'echo fs.inotify.max_user_watches=2048000 > /etc/sysctl.conf")
-- awful.util.spawn_with_shell("sudo sh -c 'echo 2048000 > /proc/sys/fs/inotify/max_user_watches'")
-- awful.util.spawn_with_shell("sleep 30; /usr/sbin/systemctl --user restart syncthing-inotify")
-- awful.util.spawn_with_shell("synclient Touchpadoff=1")
-- run_once("sleep 100; locker.lua")
-- run_once("sleep 50; dropbox-cli start")
