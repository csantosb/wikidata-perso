---
format: Org
categories:
toc: yes
title:
content:
...

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:


- [[#addons][Addons]]
  - [[#security][Security]]
    - [[#tracking][Tracking]]
    - [[#exexceptions][ExExCeptions]]
- [[#searching][Searching]]

* Addons

** Navigating

*** Vimium

**** Search engines

https://github.com/philc/vimium/wiki/Search-Engines
https://github.com/philc/vimium/wiki/Search-Completion

Qwant

#+begin_src sh :tangle search.vimium
  q: https://www.qwant.com/?q=%s&t=web Qwant
  ql: https://lite.qwant.com/?safesearch=0&suggests=false&s=0&a=1&q=%s&t=web QwantLite
#+end_src

Arch

#+begin_src sh :tangle search.vimium
  aw https://wiki.archlinux.org/index.php?search=%s ArchWiki
#+end_src

Wikipedia

#+begin_src sh :tangle search.vimium
  wes: https://es.wikipedia.org/w/index.php?title=Special:Search&search=%s Wikipedia-Es
  wen: https://en.wikipedia.org/w/index.php?title=Special:Search&search=%s Wikipedia-En
#+end_src

Google

#+begin_src sh :tangle search.vimium
  g: https://www.google.com/search?q=%s Google
  # l: https://www.google.com/search?q=%s&btnI I'm feeling lucky...
  l: https://www.google.com/search?btnI&q=%s I'm feeling lucky...
  gm: https://www.google.com/maps?q=%s Google maps
#+end_src

Youtube

#+begin_src sh :tangle search.vimium
  y: https://www.youtube.com/results?search_query=%s Youtube
#+end_src

DuckDuckGo

#+begin_src sh :tangle search.vimium
  # d: https://duckduckgo.com/?q=%s DuckDuckGo
  d: https://duckduckgo.com/?ia=about&q=%s DuckDuckGo
#+end_src

Amazon

#+begin_src sh :tangle search.vimium
  # az: https://www.amazon.com/s/?field-keywords=%s Amazon
  az: http://www.amazon.co.uk/s/?field-keywords=%s Amazon
#+end_src

#+begin_src sh :tangle search.vimium
  # Dictionary (Merriam-Webster).
  dw: http://www.merriam-webster.com/dictionary/%s Merriam-Webster
#+end_src

Bing

#+begin_src sh :tangle search.vimium
  b: https://www.bing.com/search?q=%s Bing
#+end_src

**** Keys

 - Miscellaneous

  ?  :: Show help (showHelp)
  gs :: View page source (toggleViewSource)

 - Navigating history

  H  :: Go back in history (goBack)
  L  :: Go forward in history (goForward)

 - Manipulating tabs

  t               Create new tab (createTab)

  ^               Go to previously-visited tab (visitPreviousTab)
  g0              Go to the first tab (firstTab)
  g$              Go to the last tab (lastTab)
  yt              Duplicate current tab (duplicateTab)
  Pin or unpin current tab (togglePinTab)
  <a-m>           Mute or unmute current tab (toggleMuteTab)

  X               Restore closed tab (restoreTab)
  W               Move tab to new window (moveTabToNewWindow)
  Close tabs on the left (closeTabsOnLeft)
  Close tabs on the right (closeTabsOnRight)
  Close all other tabs (closeOtherTabs)
  <<              Move tab to the left (moveTabLeft)
  >>              Move tab to the right (moveTabRight)

***** Search

https://github.com/philc/vimium/wiki/Tips-and-Tricks#keyboard-shortcuts-for-custom-search-engines

#+begin_src sh :tangle keys.vimium
  map sq Vomnibar.activate keyword=q
  map sQ Vomnibar.activateInNewTab keyword=q
  map sg Vomnibar.activate keyword=g
  map sG Vomnibar.activateInNewTab keyword=g
#+end_src

***** Tabs

#+begin_src sh :tangle keys.vimium
  map <a-n> nextTab
  map <a-p> previousTab
  map d removeTab
  map u restoreTab
#+end_src

 - Using the vomnibar

Using the vomnibar
  o               Open URL, bookmark or history entry (Vomnibar.activate)
  O               Open URL, bookmark or history entry in a new tab (Vomnibar.activateInNewTab)
  b               Open a bookmark (Vomnibar.activateBookmarks)
  B               Open a bookmark in a new tab (Vomnibar.activateBookmarksInNewTab)
  T               Search through your open tabs (Vomnibar.activateTabSelection)
  gE              Edit the current URL and open in a new tab (Vomnibar.activateEditUrlInNewTab)
  Using find
  /               Enter find mode (enterFindMode)
  n               Cycle forward to the next find match (performFind)
  N               Cycle backward to the previous find match (performBackwardsFind)

#+begin_src sh :tangle keys.vimium
  map <c-e> Vomnibar.activateEditUrl
  map <c-E> activateEditUrlInNewTab
#+end_src

 - Unmap defaults

#+begin_src sh :tangle keys.vimium
  unmap ge
  unmap gE
  unmap x
  unmap X
  unmap J
  unmap gt
  unmap gT
  unmap K
  unmap J
  unmap <c-y>
#+end_src

#+begin_src sh
  zH              Scroll all the way to the left (scrollToLeft)
  zL              Scroll all the way to the right (scrollToRight)

  yy              Copy the current URL to the clipboard (copyCurrentUrl)
  p               Open the clipboard's URL in the current tab (openCopiedUrlInCurrentTab)
  P               Open the clipboard's URL in a new tab (openCopiedUrlInNewTab)

  v               Enter visual mode (enterVisualMode)
  V               Enter visual line mode (enterVisualLineMode)

  Pass the next key to the page (passNextKey)
  gi              Focus the first text input on the page (focusInput)
  f               Open a link in the current tab (LinkHints.activateMode)
  F               Open a link in a new tab (LinkHints.activateModeToOpenInNewTab)
  Open a link in a new tab & switch to it (LinkHints.activateModeToOpenInNewForegroundTab)
  <a-f>           Open multiple links in a new tab (LinkHints.activateModeWithQueue)
  Download link url (LinkHints.activateModeToDownloadLink)
  Open a link in incognito window (LinkHints.activateModeToOpenIncognito)
  yf              Copy a link URL to the clipboard (LinkHints.activateModeToCopyLinkUrl)
  [[              Follow the link labeled previous or < (goPrevious)
   ]]               Follow the link labeled next or > (goNext)
  gf              Select the next frame on the page (nextFrame)
  gF              Select the page's main/top frame (mainFrame)
  m               Create a new mark (Marks.activateCreateMode)
  `               Go to a mark (Marks.activateGotoMode)
#+end_src

** Security

*** Tracking

https://support.mozilla.org/en-US/kb/tracking-protection-pbm?as=u

*** ExExCeptions

- https://addons.mozilla.org/fr/firefox/addon/exexceptions/
- http://www.dsfc.net/internet/tracking/un-bloqueur-publicitaire-integre-a-firefox/

* Searching

In menu bar:

 - Settings -> Search -> Search Engine -> Keyword
 - “Add a keyword for this search ...” to bookmarks
 - Qwant qwicks
