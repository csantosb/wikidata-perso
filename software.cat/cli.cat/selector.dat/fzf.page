---
format: Org
categories: software cli
toc: yes
title: fzf
content: fzf and fzf-extras
...

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[#introduction][Introduction]]
- [[#documentation][Documentation]]
  - [[#fzf][fzf]]
  - [[#fzf-extras][fzf-extras]]
- [[#install][Install]]
- [[#use][Use]]
- [[#references][References]]

* Introduction

=fzf= is a general-purpose command-line fuzzy finder.

=fzf-extras= provide additional key bindings for =fzf=.

* Documentation

** fzf

 - Online [[https://github.com/junegunn/fzf][site]]
 - Local documentation at [[/usr/share/doc/fzf/README.md]]
 - Man [[man:fzf][page]]

** fzf-extras

 - Online [[https://github.com/atweiden/fzf-extras][site]]
 - Local documentation [[/usr/share/doc/fzf-extras/README.md]]

* Install

I use [[https://www.archlinux.org/packages/community/x86_64/fzf/][fzf]] and [[https://aur.archlinux.org/packages/fzf-extras/][fzf-extras]] in to install them.

* Use

I deal with it in =zsh= [[file:/software.cat/zsh.cat/zsh#fzf][page]].

 - A completing-function is available and installed under

     [[/usr/share/fzf/completion.zsh]]

   and [[https://wiki.archlinux.org/index.php/Autostarting#.2Fetc.2Fprofile][sourced]] [[file:/software.cat/zsh.cat/zsh#zshrc-fzf][here]]

 - Key bindings are defined in

     [[/usr/share/fzf/key-bindings.zsh]]

   and rebind [[file:/software.cat/zsh.cat/zsh#fzf-binds][here]]

   They provide:

     + Alt-C  :: cd
     + Ctrl-R :: history
     + Ctrl-T :: obtain file path

 - Env variables are defined [[file:/software.cat/zsh.cat/zsh#env-fzf][here]]

 - Additional custom functions appear under my own plugin [[file:/software.cat/zsh.cat/zsh-plugin-fzf#Introduction][here]].

 - =fzf-extras= plugins are installed in [[/etc/profile.d/fzf-extras.zsh]]

   and rebind [[file:/software.cat/zsh.cat/zsh#fzf-binds][here]]

   They provide:

     + Alt-I :: locate (in extras)

* References

 - [[https://monades.roperzh.com/weekly-command-fuzzy-finding-everything-with-fzf/][Weekly Command: Fuzzy finding everything with fzf]]
