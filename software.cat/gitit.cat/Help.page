---
format: Org
categories: haskell math
toc: yes
title: Help Page
content: This page summarizes the formatting capabilities of org / pandoc / gitit
...

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

 - [[#test][Test]]
 - [[#org-markdown][Org Markdown]]
   - [[#headings][Headings]]
   - [[#code-blocks][Code blocks]]
   - [[#lists][Lists]]
   - [[#lines][Lines]]
   - [[#formatting][Formatting]]
   - [[#html-tables][Html Tables]]
   - [[#org-tables][Org Tables]]
   - [[#links][Links]]
     - [[#a-external-to-the-wiki-absolute-from-][A. External to the wiki, absolute from ~]]
     - [[#b-links-to-other-wiki-pages][B. Links to other wiki pages]]
     - [[#c-external-to-the-wiki-relative-from-project-root][C. External to the wiki, relative from project root]]
     - [[#external-links][External Links]]
     - [[#internal-page-links][Internal page links]]
     - [[#misc][Misc]]
   - [[#references][References]]
 - [[#gitit-use][Gitit Use]]
   - [[#navigating][Navigating]]
   - [[#creating-and-modifying-pages][Creating and modifying pages]]
     - [[#registering-for-an-account][Registering for an account]]
     - [[#editing-a-page][Editing a page]]
     - [[#page-metadata][Page metadata]]
     - [[#creating-a-new-page][Creating a new page]]
     - [[#reverting-to-an-earlier-version][Reverting to an earlier version]]
     - [[#deleting-a-page][Deleting a page]]
   - [[#uploading-files][Uploading files]]

* Test

* Org Markdown

** Headings

For headings, prefix a line with one or more =*= signs: one for a major
heading, two for a subheading, three for a subsubheading. Be sure to
leave space before and after the heading.

#+BEGIN_EXAMPLE
    * Markdown

    Text...

    ** Some examples...

    Text...
#+END_EXAMPLE

** Code blocks

#+BEGIN_QUOTE
  Indented quotation
#+END_QUOTE

#+BEGIN_EXAMPLE
  #include <stdbool.h>
#+END_EXAMPLE

#+begin_src lisp
  (defun botsbuildbots () (botsbuildbots))
#+end_src

#+BEGIN_SRC haskell
  let a = 1:a in head a
#+END_SRC

- : alias m ...

** Lists

-  bulleted
-  list

-  term   :: definition
-  orange :: orange fruit


1. ordered
2. list
   1. sub
      + sub-sub list  :: explanation
      + one more      :: explanation
   2. list
      - toto
      - titi
3. item three

** Lines

A line consisting of only dashes, and at least 5 of them, will be
--------------
exported as a horizontal line

Additionally, a line break\\
can be forced with two backslashes at the end of the line

** Formatting

- /italics/
- *bold*
- super^{script}
- sub_{script}
- +strike-throught+
- =verbatim=
- _underlined_
- ~code~
- =*emphasized text*=
- =**strong emphasis**=
- *strong emphasis*
- =`literal text`=
- =literal text=
- =\*escaped special characters\*=
- *escaped special characters*

** Html Tables

#+BEGIN_HTML
  <table>
  <tr>
  <td>
#+END_HTML

=*emphasized text*=

#+BEGIN_HTML
  </td>
  <td>
#+END_HTML

/emphasized text2/

#+BEGIN_HTML
  </td>
  </tr>
  <tr>
  <td>
#+END_HTML

=**strong emphasis**=

#+BEGIN_HTML
  </td>
  <td>
#+END_HTML

*strong emphasis*

#+BEGIN_HTML
  </td>
  </tr>
  <tr>
  <td>
#+END_HTML

=`literal text`=

#+BEGIN_HTML
  </td>
  <td>
#+END_HTML

=literal text=

#+BEGIN_HTML
  </td>
  </tr>
  <tr>
  <td>
#+END_HTML

=\*escaped special characters\*=

#+BEGIN_HTML
  </td>
  <td>
#+END_HTML

*escaped special characters*

#+BEGIN_HTML
  </td>
  </tr>
  <tr>
  <td>
#+END_HTML

=[external link](http://google.com)=

#+BEGIN_HTML
  </td>
  <td>
#+END_HTML

[[http://google.com][external link]]

#+BEGIN_HTML
  </td>
  </tr>
  <tr>
  <td>
#+END_HTML

=![folder](/img/icons/folder.png)=

#+BEGIN_HTML
  </td>
  <td>
#+END_HTML

[[/img/icons/folder.png]]

#+BEGIN_HTML
  </td>
  </tr>
  <tr>
  <td>
#+END_HTML

Wikilink: =[[][Front Page]]=

#+BEGIN_HTML
  </td>
  <td>
#+END_HTML

Wikilink: [[Front Page][]]

#+BEGIN_HTML
  </td>
  </tr>
  <tr>
  <td>
#+END_HTML

=H~2~O=

#+BEGIN_HTML
  </td>
  <td>
#+END_HTML

H_{2}O

#+BEGIN_HTML
  </td>
  </tr>
  <tr>
  <td>
#+END_HTML

=10^100^=

#+BEGIN_HTML
  </td>
  <td>
#+END_HTML

10^{100}

#+BEGIN_HTML
  </td>
  </tr>
  <tr>
  <td>
#+END_HTML

=~~strikeout~~=

#+BEGIN_HTML
  </td>
  <td>
#+END_HTML

+strikeout+

#+BEGIN_HTML
  </td>
  </tr>
  <tr>
  <td>
#+END_HTML

=$x = \frac{{ - b \pm \sqrt {b^2 - 4ac} }}{{2a}}$=

#+BEGIN_HTML
  </td>
  <td>
#+END_HTML

$x = \frac{{ - b \pm \sqrt {b^2 - 4ac} }}{{2a}}$ [1]

#+BEGIN_HTML
  </td>
  </tr>
  <tr>
  <td>
#+END_HTML

=A simple footnote.^[Or is it so simple?]=

#+BEGIN_HTML
  </td>
  <td>
#+END_HTML

A simple footnote. [2]

#+BEGIN_HTML
  </td>
  </tr>
  <tr>
  <td>
  <pre>
  > an indented paragraph,
  > usually used for quotations
  </pre>
  </td>
  <td>
#+END_HTML

#+BEGIN_QUOTE
  an indented paragraph, usually used for quotations
#+END_QUOTE

#+BEGIN_HTML
  </td>
  <tr>
  <td>
  <pre>
      #!/bin/sh -e
      # code, indented four spaces
      echo "Hello world"
  </pre>
  </td>
  <td>
#+END_HTML

#+BEGIN_EXAMPLE
    #!/bin/sh -e
    # code, indented four spaces
    echo "Hello world"
#+END_EXAMPLE

#+BEGIN_HTML
  </td>
  </tr>
  <tr>
  <td>
  <pre>
  * a bulleted list
  * second item
      - sublist
      - and more
  * back to main list
      1. this item has an ordered
      2. sublist
          a) you can also use letters
          b) another item
  </pre>
  </td>
  <td>
#+END_HTML

-  a bulleted list
-  second item

   -  sublist
   -  and more

-  back to main list

   1. this item has an ordered
   2. sublist

      1) you can also use letters
      2) another item

#+BEGIN_HTML
  </td>
  </tr>
  </table>
#+END_HTML

** Org Tables

#+BEGIN_HTML
  <table>
  <tr>
  <td>
  <pre>
  Fruit        Quantity
  --------  -----------
  apples         30,200
  oranges         1,998
  pears              42

  Table:  Our fruit inventory
  </pre>
  </td>
  <td>
#+END_HTML

| Fruit     | Quantity   |
|-----------+------------|
| apples    | 30,200     |
| oranges   | 1,998      |
| pears     | 42         |
#+CAPTION: Our fruit inventory

#+BEGIN_HTML
  </td>
  </tr>
  </table>
#+END_HTML

** Links

Majuscules / minuscules

*** A. External to the wiki, absolute from ~

Meaningless from gitit

 - To an arbitrary file plus a heading :: [[file:~/Projects/perso/wikidata-perso/emacs.cat/org-config.cat/csb-elfeed-feeds.org#Politics][csb-elfeed-feeds.org]]

   #+begin_example
     [[file:~/Projects/perso/wikidata-perso/emacs.cat/org-config.cat/csb-elfeed-feeds.org#Politics][csb-elfeed-feeds.org]]
   #+end_example

 - To an arbitrary file without heading :: [[file:~/Projects/perso/wikidata-perso/emacs.cat/org-config.cat/csb-elfeed-feeds.org][csb-elfeed-feeds.org]]

   #+begin_example
     [[file:~/Projects/perso/wikidata-perso/emacs.cat/org-config.cat/csb-elfeed-feeds.org][csb-elfeed-feeds.org]]
   #+end_example

*** B. Links to other wiki pages

 - To a wiki page plus a heading :: [[file:/emacs.cat/org-config.cat/csb-vc#launcher][csb-vc#launcher]]

   #+begin_example
     [[/emacs.cat/org-config.cat/csb-vc#launcher][csb-vc#launcher]]
   #+end_example

 - To a wiki page without heading :: [[file:/emacs.cat/org-config.cat/csb-vc][csb-vc]]

   #+begin_example
     [[/emacs.cat/org-config.cat/csb-vc][csb-vc]]
   #+end_example

*** C. External to the wiki, relative from project root

Meaningless from gitit

 - To an arbitrary file plus a heading :: [[file:~/Projects/perso/wikidata-perso/emacs.cat/org-config.cat/csb-elfeed-feeds.org#Politics][csb-elfeed-feeds.org]]

   #+begin_example
     [[file:~/Projects/perso/wikidata-perso/emacs.cat/org-config.cat/csb-elfeed-feeds.org#Politics][csb-elfeed-feeds.org]]
   #+end_example

 - To an arbitrary file without heading :: [[file:~/Projects/perso/wikidata-perso/emacs.cat/org-config.cat/csb-elfeed-feeds.org][csb-elfeed-feeds.org]]

   #+begin_example
     [[file:~/Projects/perso/wikidata-perso/emacs.cat/org-config.cat/csb-elfeed-feeds.org][csb-elfeed-feeds.org]]
   #+end_example

*** External Links

As usual with org : [[http://www.google.com][Google]]

*** Internal page links

Links to another section of the same page

[[#Test][Headings]]

[[*Test][Headings]]

[[*Internal page links][Internal page links]

*** Misc

[[squashing][this is a link]]  -->  <<squashing>> [[file:~/Projects/wikidata/bookmarks.cat/bookmarks.page::*Squashing%20Commits%20in%20Git][Squashing Commits in Git]]

** References

- For more about org in general, check the [[http://orgmode.org/manual/][org-mode-manual]].

- Two source file in the pandoc git source tree include a summary of capabilities. They can be found [[http://hdiff.luite.com/cgit/gitit/tree/data/markupHelp/Org][here]] and [[http://hdiff.luite.com/cgit/gitit/tree/data/markup.Org][here]].

- Consult the [[http://johnmacfarlane.net/pandoc/README.html][pandoc User's Guide]] for information about pandoc's syntax for footnotes, tables,
  description lists, and other elements not present in standard markdown.

* Gitit Use

** Navigating

The most natural way of navigating is by clicking wiki links that
connect one page with another. The "Front page" link in the navigation
bar will always take you to the Front Page of the wiki. The "All pages"
link will take you to a list of all pages on the wiki (organized into
folders if directories are used). Alternatively, you can search using
the search box. Note that the search is set to look for whole words, so
if you are looking for "gremlins", type that and not "gremlin". The "go"
box will take you directly to the page you type.

** Creating and modifying pages

*** Registering for an account

In order to modify pages, you'll need to be logged in. To register for
an account, just click the "register" button in the bar on top of the
screen. You'll be asked to choose a username and a password, which you
can use to log in in the future by clicking the "login" button. While
you are logged in, these buttons are replaced by a "logout so-and-so"
button, which you should click to log out when you are finished.

Note that logins are persistent through session cookies, so if you don't
log out, you'll still be logged in when you return to the wiki from the
same browser in the future.

*** Editing a page

To edit a page, just click the "edit" button at the bottom right corner
of the page.

You can click "Preview" at any time to see how your changes will look.
Nothing is saved until you press "Save."

Note that you must provide a description of your changes. This is to
make it easier for others to see how a wiki page has been changed.

*** Page metadata

Pages may optionally begin with a metadata block. Here is an example:

#+BEGIN_EXAMPLE
    ---
    format: latex+lhs
    categories: haskell math
    toc: no
    title: Haskell and
      Category Theory
    ...

    \section{Why Category Theory?}
#+END_EXAMPLE

The metadata block consists of a list of key-value pairs, each on a
separate line. If needed, the value can be continued on one or more
additional line, which must begin with a space. (This is illustrated by
the "title" example above.) The metadata block must begin with a line
=---= and end with a line =...= optionally followed by one or more blank
lines.

Currently the following keys are supported:

-  format :: Overrides the default page type as specified in the
   configuration file. Possible values are =markdown=, =rst=, =latex=,
   =html=, =markdown+lhs=, =rst+lhs=, =latex+lhs=. (Capitalization is
   ignored, so you can also use =LaTeX=, =HTML=, etc.) The =+lhs=
   variants indicate that the page is to be interpreted as literate
   Haskell. If this field is missing, the default page type will be
   used.
-  categories :: A space or comma separated list of categories to which
   the page belongs.
-  toc :: Overrides default setting for table-of-contents in the
   configuration file. Values can be =yes=, =no=, =true=, or =false=
   (capitalization is ignored).
-  title :: By default the displayed page title is the page name. This
   metadata element overrides that default.

*** Creating a new page

To create a new page, just create a wiki link that links to it, and
click the link. If the page does not exist, you will be editing it
immediately.

*** Reverting to an earlier version

If you click the "history" button at the bottom of the page, you will
get a record of previous versions of the page. You can see the
differences between two versions by dragging one onto the other;
additions will be highlighted in yellow, and deletions will be crossed
out with a horizontal line. Clicking on the description of changes will
take you to the page as it existed after those changes. To revert the
page to the revision you're currently looking at, just click the
"revert" button at the bottom of the page, then "Save".

*** Deleting a page

The "delete" button at the bottom of the page will delete a page. Note
that deleted pages can be recovered, since a record of them will still
be accessible via the "activity" button on the top of the page.

** Uploading files

To upload a file--a picture, a PDF, or some other resource--click the
"upload" button in the navigation bar. You will be prompted to select
the file to upload. As with edits, you will be asked to provide a
description of the resource (or of the change, if you are overwriting an
existing file).

Often you may leave "Name on wiki" blank, since the existing name of the
file will be used by default. If that isn't desired, supply a name. Note
that uploaded files /must/ include a file extension (e.g. =.pdf=).

If you are providing a new version of a file that already exists on the
wiki, check the box "Overwrite existing file." Otherwise, leave it
unchecked.

To link to an uploaded file, just use its name in a regular wiki link.
For example, if you uploaded a picture =fido.jpg=, you can insert the
picture into a (markdown-formatted) page as follows:
=![fido](fido.jpg)=. If you uploaded a PDF =projection.pdf=, you can
insert a link to it using: =[projection](projection.pdf)=.

-  Markdown

This wiki's pages are written in
[[http://johnmacfarlane.net/pandoc][pandoc]]'s extended form of
[[http://daringfireball.net/projects/markdown][markdown]]. If you're not
familiar with markdown, you should start by looking at the
[[http://daringfireball.net/projects/markdown/basics][markdown "basics"
page]] and the
[[http://daringfireball.net/projects/markdown/syntax][markdown syntax
description]]. Consult the
[[http://johnmacfarlane.net/pandoc/README.html][pandoc User's Guide]]
for information about pandoc's syntax for footnotes, tables, description
lists, and other elements not present in standard markdown.

Markdown is pretty intuitive, since it is based on email conventions.
Here are some examples to get you started:

#+BEGIN_HTML
  <table>
#+END_HTML

#+BEGIN_HTML
  <tr>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

=/emphasized text/=

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

/emphasized text/

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  </tr>
#+END_HTML

#+BEGIN_HTML
  <tr>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

=*strong emphasis*=

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

/strong emphasis/

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  </tr>
#+END_HTML

#+BEGIN_HTML
  <tr>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

==literal text==

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

=literal text=

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  </tr>
#+END_HTML

#+BEGIN_HTML
  <tr>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

=*escaped special characters*=

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

/escaped special characters/

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  </tr>
#+END_HTML

#+BEGIN_HTML
  <tr>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

=[[http://google.com][external link]]=

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

[[http://google.com][external link]]

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  </tr>
#+END_HTML

#+BEGIN_HTML
  <tr>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

=[[/img/icons/folder.png]]=

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

[[/img/icons/folder.png]]

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  </tr>
#+END_HTML

#+BEGIN_HTML
  <tr>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

Wikilink: =[[][Front Page]]=

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

Wikilink: [[][Front Page]]

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  </tr>
#+END_HTML

#+BEGIN_HTML
  <tr>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

=H_{2}O=

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

H\_{2}O

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  </tr>
#+END_HTML

#+BEGIN_HTML
  <tr>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

=10^{100}=

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

10\^{100}

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  </tr>
#+END_HTML

#+BEGIN_HTML
  <tr>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

=+strikeout+=

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

+strikeout+

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  </tr>
#+END_HTML

#+BEGIN_HTML
  <tr>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

=$x = \frac{{ - b \pm \sqrt {b^2 - 4ac} }}{{2a}}$=

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

$x = \frac{{ - b \pm \sqrt {b^2 - 4ac} }}{{2a}}$ [1]

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  </tr>
#+END_HTML

#+BEGIN_HTML
  <tr>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

=A simple footnote. [1]=

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

A simple footnote. [2]

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  </tr>
#+END_HTML

#+BEGIN_HTML
  <tr>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

#+BEGIN_HTML
  <pre>
    > an indented paragraph,
    > usually used for quotations
    </pre>
#+END_HTML

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

an indented paragraph, usually used for quotations #+END\_QUOTE

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  <tr>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

#+BEGIN_HTML
  <pre>
        #!/bin/sh -e
        # code, indented four spaces
        echo "Hello world"
    </pre>
#+END_HTML

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

#+BEGIN_EXAMPLE
    #!/bin/sh -e
    # code, indented four spaces
    echo "Hello world"
#+END_EXAMPLE

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  </tr>
#+END_HTML

#+BEGIN_HTML
  <tr>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

#+BEGIN_HTML
  <pre>
    * a bulleted list
    * second item
        - sublist
        - and more
    * back to main list
        1. this item has an ordered
        2. sublist
            a) you can also use letters
            b) another item
    </pre>
#+END_HTML

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

-  a bulleted list
-  second item

-  sublist
-  and more

-  back to main list

1. this item has an ordered
2. sublist

   1) you can also use letters
   2) another item

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  </tr>
#+END_HTML

#+BEGIN_HTML
  <tr>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

#+BEGIN_HTML
  <pre>
    Fruit        Quantity
    --------  -----------
    apples         30,200
    oranges         1,998
    pears              42

    Table:  Our fruit inventory
    </pre>
#+END_HTML

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  <td>
#+END_HTML

| Fruit | Quantity | |-----------+------------| | apples | 30,200 | |
oranges | 1,998 | | pears | 42 | #+CAPTION: Our fruit inventory

#+BEGIN_HTML
  </td>
#+END_HTML

#+BEGIN_HTML
  </tr>
#+END_HTML

#+BEGIN_HTML
  </table>
#+END_HTML
